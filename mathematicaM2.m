(* ::Package:: *)

(* MathematicaM2 package *)
(* Yang Zhang *)

BeginPackage["mathematicaM2`"]

LT::usage ="Obtain the leading terms of an ideal."
Monic::usage="Normalize a polynomial to a monic polynomial in a given monomial order." 


MonomialCoefficients::usage="Find the coefficients of a polynomial over a given monomial basis."

monomialAnsatz::usage="Find all monomials in variables whose total degree is lower or equal a given limit."

GroebnerLinearSolve::usage="Solve a linear system via Groebner basis method."

PrimaryDecomposition::usage =
        "Primary decomposition."
FractionReduce::usage =
        "Reduce a fraction to a polynomial plus terms in the ideal, where the latter would not be shown."
recursiveReduce::usage="Reduce a polynomial towards a set of divisors with high-order expansion."

FullDivision::usage="Reduce a polynomial towards a set of divisors with high-order expansion, from lower order to higher order."


ProjectiveIdeal::usage ="This generates the projective ideal of a given affine ideal."
NaiveProjectiveIdeal::usage ="This generates a projective ideal from the generator sets of a given affine ideal. This may NOT correspond to the minimal project closure of an affine variety!"

M2GBasis::usage="Using the testing code of Macaulay2 to calculate a Groebner basis with coefficients in finite field."
M2GroebnerBasis::usage="Calculate Groebner basis using Macaulay2. It is possible to generate the generating matrix for a Groebner basis."
M2PolynomialReduce::usage="Polynomial reduction"
M2SingularLocus::usage ="This generates the equtions for the singular points on a variety. The exact location of singular points can be determined by solving these equations."
M2Intersect::usage ="Insersection of two Ideals."
M2ArithmeticGenus::usage="Arithmetic genus of an Ideal."
M2Dim::usage="Dimension of a variety."
M2Syzygy::usage="Calculates the syzygy of the given generator set of an ideal."
M2Syz::usage="Calculates the syzygy of the given generator set of an ideal, without the explicit form of Groebner basis."
M2Degree::usage="Degree of a variety."



M2Radical::usage="This calculates the radical ideal."
QuotientRingBasis::usage="This provides the linear basis for a zero-dimensional variety."
Bezoutian::usage="This calculates the Bezoutian matrix."
GlobalDual::usage="This calculates the dual form of a global basis."


CRT2::usage="Chinese remainder theorem for two polynomials."
quickCRT::usage="Chinese remainder theorem for two polynomials, quick mode."
CRTprepare::usage="Prepare the basis for Chinese remainder theorem."


QuadraticForm::usage="Quadratic Form Reduction."
ResultantMatrix::usage="Make the resultant matrix for two polynomials in one variable. The determinant of this matrix is the resultant."
PolynomialDiophantine::usage="Solve for the polynomial Dophantine equation h1 f1+ h2 f2= h. The input is PolynomialDiophantine[f1,f2,h,x]";


Elimination::usage="Elimate variables from an ideal."
Implicitization::usage="Find the variety defined by parameters."
IntersectIdeal::usage="Intersection of several Ideals. It may be slow for the case with a large number of ideals."
CRT::usage="Chinese remainder theorem for several polynomials. It may be slow for the case with a large number of polynomials."
Diff::usage="Generate the Jacobian."
PolynomialDegree::usage="Find the degree of a polynomial."
FindFactor::usage="Find one irreducible factor of a polynomial, over C."
Factorize::usage="Find all irreducible factors of a polynomial, over C."
ListSolve::usage="Solve a list of equations."
PolynomialProduct::usage="Find the product of two polynomials and then divide by the Grobner Basis."
PolynomialPower::usage="Find the power of a polynomial and then divide by the Grobner Basis."

CompareIdeal::usage="This compares two ideals." 
SubIdeal::usage="This tests if ideal 1 is inside ideal 2."

PolynomialDivision::usage="This calculates the polynomial division, with the quotients on the original numerators."
TangentCone::usage="This generates the tangent cone at one point."
SingularPoints::usage="This analyzes the singular points of a projective curve."
CurveTangentLine::usage="This gives the tangenent lines of a plane curve at one point."
CurveAnalysis::usage="This analyzes the singular points of a plane curve numerically."

Transformation1::usage="This transforms an ideal."
Transformation2::usage="This transforms an ideal."
UnivariateResidue::usage="This calcuates a univariate residue"
MultiResidue::usage="This calculates a multivariate or univariate residue, with the standard numerator-and-divisors form."
residue::usage="This calculates a multivariate or univariate residue, with the fraction-and-divisors form."

CharacterFunction::usage="This generates a function which takes different values on different points."
PartitionFunction::usage="This generates the partition functions for a given point set."
Multiplication::usage="Multiplication in a quotient ring of a zero-dimensional ideal."

BezoutianResidue::usage="This calculates a multivariate or univariate residue, with the Bezoutian matrix approach."
BezoutianGlobalResidue::usage="This calculates the sum of residues, with the Bezoutian matrix approach."


GFLift::usage="GFLift[n,p] lift n in Z/p to a rational number. When the lift fails, the output is XXX."
polyFitting::usage="polyFitting[list1_,paralist1_,var1_,degree1_,char1_] fits polynomials' coefficients in Q or Z/p."
polyGFLift::usage="polyGFLift[f1_,var1_,p1_] lift all cofficients in a polynomial to a rational number."
CoefficientsLift::usage="CoefficientsLift[flist1_,numericlist1_,var1_,clist1_,deg1_,p1_] lifts all terms, as polynomials of coefficients, in a polynomial to a rational numbers."
fractionFitting::usage="fractionFitting[list1_,paralist1_,var1_,degree1_,degree2_,char1_] fits a rational function."
polynomialFit::usage="polynomialFit[flist1_,var1_,clist1_,numericlist1_,degree1_,degree2_,p1_] fits the coefficients of a polynomial as rational functions"


Begin["Private`"]


Global`MM2ver="0.98";


M2path=Global`M2path;
Datapath=Global`Datapath;

Print["MathematicaM2 package, version "<>Global`MM2ver<>", by Yang Zhang"];
Print["Macaulay2 path: ",M2path];
Print["Path of temporary files: ", Datapath];



(* ::Subsection:: *)
(*General Functions*)


ListSolve[list_,Vlist_]:=Quiet[Solve[list==Table[0,{i,1,Length[list]}],Vlist]];
PolynomialDegree[poly_,VList_]:=Exponent[MonomialList[poly,VList,DegreeLexicographic][[1]],VList]//Total;
Diff[Ideal_,VList_]:=Table[D[Ideal[[i]],VList[[j]]],{i,1,Length[Ideal]},{j,1,Length[VList]}];

(*This function convert a monomial to a list of its powers in variables*)
ExpRead[monomial_,VarList_]:=Exponent[monomial,VarList];

(*This function convert a list of powers in variables to a monomial*)
ExpRecover[list_,VarList_]:=Inner[Power,VarList,list,Times];

Trivialization[var_]:=(#->1)&/@var;

Options[Monic]={MonomialOrder->Lexicographic};
Monic[f1_,var1_,OptionsPattern[]]:=Module[{f=f1,var=var1,MOrder=OptionValue[MonomialOrder]},
		Return[f/(MonomialList[f,var,MOrder][[1]]/.Trivialization[var])];
];


Options[GroebnerLinearSolve]={VariableList->{},IndependentVariableList->{}};
GroebnerLinearSolve[A1_,b1_,OptionsPattern[]]:=Module[{depVar,cc,A=A1,b=b1,clist,len,Eqns,ss,Var=OptionValue[VariableList],IndepVar=OptionValue[IndependentVariableList]},
				len=A[[1]]//Length;
				clist=Var;
				If[clist=={},clist=Table[cc[i],{i,1,len}]];
			
			    depVar=Complement[clist,IndepVar];
				Eqns=A.clist-b;
				Eqns=Table[Eqns[[i]]//Together//Numerator,{i,1,len}]; 
				Eqns=GroebnerBasis[Eqns,clist,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions];
				ss=ListSolve[Eqns,depVar];
				
				If[ss=={},Return[ss]];
				Return[ss[[1]]];
];


LinearCoefficient[LinearPoly_,vlist0_]:=Module[{Poly=LinearPoly,TrivialCondition,vlist=vlist0,XX,ref,ZeroCondition,A,b},
ref=XX*Total[vlist];
ZeroCondition=Table[vlist[[i]]->0,{i,1,Length[vlist]}];
TrivialCondition=Table[vlist[[i]]->1,{i,1,Length[vlist]}];
b=Poly/.ZeroCondition;
A=MonomialList[Poly+ref-b,vlist];
A=A/.XX->0/.TrivialCondition;
Return[{A,b}];
];

LeadingTermUnit[Ideal1_,Varlist1_,Morder1_]:=Module[{TrivializationCondition,i,Ideal=Ideal1,vlist=Varlist1,Morder=Morder1,list},
	TrivializationCondition=Table[vlist[[k]]->1,{k,1,Length[vlist]}];
	For[i=1,i<=Length[Ideal],i++,
		list=MonomialList[Ideal[[i]],vlist,Morder];
        Ideal[[i]]=Ideal[[i]]/(list[[1]]/.TrivializationCondition)//Simplify;	
	];
	Return[Ideal];

];


MonomialUnit[f_,var_]:=Times@@MapThread[#1^#2&,{var,Exponent[f,var]}];


Options[LT]={MonomialOrder->DegreeLexicographic};
LT[ideal1_,varList1_,OptionsPattern[]]:=Module[{ideal=ideal1,var=varList1,mOrder=OptionValue[MonomialOrder],result,Gr},

Gr=GroebnerBasis[ideal, var, MonomialOrder->mOrder,CoefficientDomain->RationalFunctions];

result=MonomialUnit[MonomialList[#,var,mOrder][[1]],var]&/@Gr;


Return[result];

];



Options[QuotientRingBasis]={MonomialOrder->DegreeLexicographic,GB->False};
QuotientRingBasis[ideal1_,varList1_,OptionsPattern[]]:=Module[{ideal=ideal1,var=varList1,mOrder=OptionValue[MonomialOrder],TrivialCondition,LTList,i,n,condition,powerList,result,ss,
GrFlag=OptionValue[GB]},
	If[GrFlag==False,LTList=LT[ideal,var,MonomialOrder->mOrder],LTList=MonomialUnit[First[MonomialList[#,var,mOrder]],var]&/@ideal];
	n=Length[var];
	
	condition={};
	For[i=1,i<=Length[LTList],i++,
		powerList=ExpRead[LTList[[i]],var];
		AppendTo[condition,Table[cc[i]<powerList[[i]],{i,1,n}]/.List->Or];

	];

	


	AppendTo[condition,Table[cc[i]>=0,{i,1,n}]/.List->And];

	ss=Solve[condition,Table[cc[i],{i,1,n}],Integers];
	result={};
	For[i=1,i<=Length[ss],i++,
		AppendTo[result,ExpRecover[Table[cc[j],{j,1,n}]/.ss[[i]],var]];

	];
	Return[MonomialList[Total[result],var,mOrder]];

];


MonomialCoefficients[f1_,varList1_,ref1_,Morder1_]:=Module[{f=f1,Trivialization,varList=varList1,ref=ref1,Morder=Morder1,h,XXX,result},
		h=f+XXX*ref;
		Trivialization=Table[varList[[i]]->1,{i,1,Length[varList]}];
		result=MonomialList[h,varList,Morder];
		result=result/.XXX->0/.Trivialization;
		Return[result];

]


PolynomialProduct[f_,g_,Gr_,Var_,MOrder_]:=PolynomialReduce[f*g,Gr,Var,MonomialOrder->MOrder,CoefficientDomain->RationalFunctions][[2]];
PolynomialPower[f1_,n1_,Gr1_,Var1_,MOrder1_]:=Module[{f=f1,n=n1,Gr=Gr1,Var=Var1,MOrder=MOrder1,i,result},
	result=f;
	For[i=2,i<=n,i++,
			result=PolynomialReduce[result*f,Gr,Var,MonomialOrder->MOrder,CoefficientDomain->RationalFunctions][[2]];
		];
	Return[result];
];


ResultantMatrix[F1_,F2_,X_]:=Module[{f=F1,g=F2,x=X,m,n,i,a,b,M},
		n=Exponent[f,x];
		m=Exponent[g,x];

		a=Table[SeriesCoefficient[f,{x,0,i}],{i,0,n}]//Reverse;
		b=Table[SeriesCoefficient[g,{x,0,i}],{i,0,m}]//Reverse;

		
		M=Join[Table[Join[ConstantArray[0,i-1],a,ConstantArray[0,m-i]],{i,1,m}],Table[Join[ConstantArray[0,i-1],b,ConstantArray[0,n-i]],{i,1,n}]];
		
		Return[M//Transpose];     
];


PolynomialDiophantine[F1_,F2_,H_,X_]:=Module[{h=H,f=F1,g=F2,x=X,m,n,M,hlist,clist,h1,h2,q=0,r},
		n=Exponent[f,x];
		m=Exponent[g,x];
		
		If[Exponent[h,x]>n+m-1,
			{{q},r}=PolynomialReduce[h,f,x,CoefficientDomain->RationalFunctions];
			h=r;
		];
		
		hlist=Table[SeriesCoefficient[h,{x,0,i}],{i,0,n+m-1}]//Reverse;
		M=ResultantMatrix[F1,F2,x];

		clist=LinearSolve[ResultantMatrix[F1,F2,x],hlist];

		h1=Take[clist,m].(Table[x^i,{i,0,m-1}]//Reverse);
		h2=Take[clist,-n].(Table[x^i,{i,0,n-1}]//Reverse);

		Return[{h1,h2}];
		
];


(* ::Subsection::Closed:: *)
(*Quadratic form  *)


Zero[x_]:=Switch[FullSimplify[x==0],True,True,_,False]
PermMatrix[i_,j_,n_]:=Module[{Id,v},
Id=IdentityMatrix[n];
v=Id[[i]];
Id[[i]]=Id[[j]];
Id[[j]]=v;
Return[Id];
]
QuadraticSkewMatrix[n_]:=Module[{Id},
If[n==1,Return[{{1}}]];
Id=IdentityMatrix[n];
Id[[1,2]]=1;
Id[[2,1]]=-1;
Return[Id];
]
CombineMatrix[M1_,M2_]:=Module[{Matrix,n1,n2,i,j},
n1=Length[M1];
n2=Length[M2];
Matrix=Table[0,{i,1,n1+n2},{j,1,n1+n2}];
For[i=1,i<=n1,i++,
For[j=1,j<=n1,j++,
Matrix[[i,j]]=M1[[i,j]];
];
];
For[i=1,i<=n2,i++,
For[j=1,j<=n2,j++,
Matrix[[i+n1,j+n1]]=M2[[i,j]];
];
];
Return[Matrix];
]
CropMatrix[M_]:=Table[M[[i,j]],{i,2,Length[M]},{j,2,Length[M]}];
SymMatrixReduce[M1_]:=Module[{M=M1,S,n,i,j,B,flag,MM,SS},
n=Length[M];
S=IdentityMatrix[n];
If[n==1,Return[{M,S}]];
If[Zero[M[[1,1]]]==False,n=n,
	For[i=2,i<=n,i++,If[Zero[M[[i,i]]]==False,Break[]];];

If[i<=n,
	S=S.PermMatrix[1,i,n]//Simplify; 
        M=Transpose[S].M.S//Simplify;
,
	flag=0;
	For[i=1,i<=n,i++,
	For[j=1,j<=n,j++,
	If[Zero[M[[i,j]]]==False,flag=1;Break[]];
	];
	If[flag==1,Break[]];
	];
	If[i>n||j>n,Return[{M,S}];];

         If[n==2,B={{1,1},{-1,1}},B=CombineMatrix[{{1,1},{-1,1}},IdentityMatrix[n-2]]];
	S=S.PermMatrix[1,i,n].PermMatrix[2,j,n].B//Simplify; 
	M=Transpose[S].M.S//Simplify;
];
];                
B=IdentityMatrix[n];
For[i=2,i<=n,i++,B[[1,i]]=-M[[1,i]]/M[[1,1]]];

S=S.B//Simplify;
M=Transpose[B].M.B//Simplify;

{MM,SS}=SymMatrixReduce[CropMatrix[M]];


M=CombineMatrix[{{M[[1,1]]}},MM];
S=S.CombineMatrix[{{1}},SS]//Simplify;
Return[{M,S}];
];
Options[QuadraticForm]={SkewForm->False,NewVariables->False,Normalization->False,RealCondition->True};
QuadraticForm[expression1_,VList1_,OptionsPattern[]]:=Module [{expression=expression1,VList=VList1,len,result,NewVList,A,b,c,NormalizationMatrix,Shift,
SkewFlag=OptionValue[SkewForm],NewV=OptionValue[NewVariables],NormalFlag=OptionValue[Normalization],M,RFlag=OptionValue[RealCondition],S,replacementRule,i,InvM,target,InvReplacementRule
,rank=0,sk},
len=Length[VList];
NewVList=NewV;
If[NewV==False,NewVList=Table[x[i],{i,1,len}]];
target=expression;

A=Table[Coefficient[target,VList[[i]]*VList[[j]]],{i,1,len},{j,1,len}]/2;
A+=DiagonalMatrix[Table[A[[i,i]],{i,1,len}]];
target=Simplify[target-VList.A.VList];
b=Table[Coefficient[target,VList[[i]]],{i,1,len}];
c=Simplify[target-b.VList];
{M,S}=SymMatrixReduce[A];
If[NormalFlag==True,
NormalizationMatrix=IdentityMatrix[len];
For[i=1,i<=len,i++,
If[Zero[M[[i,i]]]==False&&RFlag==False,NormalizationMatrix[[i,i]]=1/Sqrt[M[[i,i]]]];
If[Zero[M[[i,i]]]==False&&RFlag==True,NormalizationMatrix[[i,i]]=1/Sqrt[Abs[M[[i,i]]]]];
];
S=S.NormalizationMatrix//Simplify;
M=Transpose[NormalizationMatrix].M.NormalizationMatrix//Simplify;
];



b=b.S;
InvM=0*IdentityMatrix[len];
For[i=1,i<=len,i++,
If[Zero[M[[i,i]]]==False,InvM[[i,i]]=1/M[[i,i]]; rank++];
];
Shift=InvM.b/2;

If[SkewFlag==True,
		
sk=Table[0,{i,1,len},{j,1,len}];   
       For[i=1,i<=rank/2,i++,
		
             sk[[2i-1,2i-1]]=1/2;
			 sk[[2i-1,2i]]=1/2;
			 sk[[2i,2i-1]]=1/(2 Sqrt[-(M[[2i,2i]]/M[[2i-1,2i-1]])]);
			 sk[[2i,2i]]=-(1/(2 Sqrt[-(M[[2i,2i]]/M[[2i-1,2i-1]])]));
			];
		For[i=Floor[rank/2]*2+1,i<=len,i++,sk[[i,i]]=1];
		S=S.sk;
		Shift=Inverse[sk].Shift;
];

replacementRule=Table[NewVList[[i]]->(Inverse[S].VList+Shift)[[i]],{i,1,len}];
InvReplacementRule=Table[VList[[i]]->(S.NewVList-S.Shift)[[i]],{i,1,len}];

Return[{Expand[expression/.InvReplacementRule],replacementRule,InvReplacementRule}];

]


(* ::Subsection::Closed:: *)
(*Projective Ideal*)


ProjectiveIdeal[Ideal1_,VList1_,variable0_]:=Module[{Ideal=Ideal1,VList=VList1,variable=variable0,replacement},
Ideal=GroebnerBasis[Ideal,VList,CoefficientDomain->RationalFunctions,MonomialOrder->DegreeReverseLexicographic];
replacement=Table[VList[[i]]->VList[[i]]/variable,{i,1,Length[VList]}];
Return[Table[(Ideal[[i]]/.replacement)*variable^PolynomialDegree[Ideal[[i]],VList]//Expand,{i,1,Length[Ideal]}]];
]
NaiveProjectiveIdeal[Ideal1_,VList1_,variable0_]:=Module[{Ideal=Ideal1,VList=VList1,variable=variable0,replacement},

replacement=Table[VList[[i]]->VList[[i]]/variable,{i,1,Length[VList]}];
Return[Table[(Ideal[[i]]/.replacement)*variable^PolynomialDegree[Ideal[[i]],VList]//Expand,{i,1,Length[Ideal]}]];
]



(* Prm[polynomial1_,VarList1_,Ordering1_]:=Module[{pol=polynomial1,Var=VarList1,ord=Ordering1,i,list},list=MonomialList[pol,Var,ord];
list=Table[Mrm[list[[i]],Var],{i,1,Length[list]}];
Return[list];]
RemoveCoefficient[poly0_,VarList0_]:=Module[{exp,list,len,ParaList,poly=poly0,VarList=VarList0,Replace,list1,i,result=1,Vlist},exp=Numerator[Together[poly]];
If[exp==0,Return[0];];
ParaList=Complement[Variables[exp],VarList];
Replace=Table[ParaList[[i]]->0,{i,1,Length[ParaList]}];
list=FactorTermsList[exp,VarList];
len=Length[list];
For[i=1,i<=len,i++,Vlist=Complement[Variables[list[[i]]],ParaList];
If[Vlist=={},Continue[]];
result*=list[[i]];];
Return[result];];
PolyDiv[F1_,Ideal1_,VarList1_,Morder1_]:=Module[{F=F1,Ideal=Ideal1,VarList=VarList1,Morder=Morder1,Gr,r},Gr=GroebnerBasis[Ideal,VarList,MonomialOrder->Morder,CoefficientDomain->RationalFunctions];
r=PolynomialReduce[F,Gr,VarList,MonomialOrder->Morder][[2]];
Return[r//Simplify];];
IdealRemoveCoefficient[Ideal_,VarList_]:=Table[RemoveCoefficient[Ideal[[i]],VarList],{i,1,Length[Ideal]}]; *)



(* ::Subsection:: *)
(*Macaulay2 Preparation *)


Mrm[monomial_,VarList_]:=Inner[Power,VarList,Exponent[monomial,VarList],Times];


nameGen[str_,n_]:=Table[ToExpression[str<>ToString[i]],{i,1,n}];

variableNames[vlist_,clist_]:=Join[MapThread[#1->#2&,{vlist,nameGen["x",Length[vlist]]}],MapThread[#1->#2&,{clist,nameGen["c",Length[clist]]}]];
variableNamesBack[vlist_,clist_]:=Join[MapThread[#2->#1&,{vlist,nameGen["x",Length[vlist]]}],MapThread[#2->#1&,{clist,nameGen["c",Length[clist]]}]]


(*  Old-fashion Mathematica \[Rule] M2 conversion

Poly2String[Polynomial1_,VarList1_]:=Module[{VList=VarList1,mlist,Poly=Polynomial1,len,i,x,c,string="",string1},
If[Simplify[Poly]==0,Return["0"]];
mlist=MonomialList[Expand[Poly],VList,DegreeLexicographic];
len=Length[mlist];
For[i=1,i<=len,i++,x=Mrm[mlist[[i]],VList];
c=mlist[[i]]/x;
string1="("<>ToString[c,InputForm]<>")"<>"*";
If[c==1,string1=""];
string=string<>string1<>ToString[x,InputForm];
If[i<len,string=string<>"+"];];
Return[string];]

*)


(* 
Poly2String[Polynomial1_,VarList1_]:= Module[{VList=VarList1,Poly=Polynomial1,list,clist,mlist,str,TC},
	list=MonomialList[Poly,VList,DegreeLexicographic];
	If[list=={0},Return["0"]];

	TC=(#->1)&/@VList;	
	mlist=Mrm[#,VList]&/@list;
	clist=list/.TC;

	str=ToString[InputForm[(HoldForm/@clist).mlist]/.HoldForm[1]->1];
	Return[StringReplace[str,"HoldForm["~~Shortest[x___]~~"]":>"("~~x~~")"]];
];
*)


Poly2String[Polynomial1_,VarList1_]:= Module[{VList=VarList1,Poly=Polynomial1,cr,cr1,string},
	cr=CoefficientRules[Poly,VList];
	cr1=(#[[1]]->Global`Co[#[[2]]])&/@cr;

	string=ToString[FromCoefficientRules[cr1,VList]//InputForm];
	Return[StringReplace[string,"Co["~~Shortest[x___]~~"]":>"("~~x~~")"]];

];


Ring2String[list_,Field_]:=StringReplace[Field<>ToString[list],{"{"->"[","}"->"]"}];


(*Ideal2String[Ideal1_,VarList1_]:=Module[{VList=VarList1,CList,mlist,Ideal=Ideal1,len,i,x,c,string="(",variables},

len=Length[Ideal];
CList=Complement[Variables[Ideal],VList];
variables=VList/.variableNames[VList,CList];



For[i=1,i<=len,i++,string=string<>Poly2String[Ideal[[i]]/.variableNames[VList,CList],variables];
If[i<len,string=string<>","];];
string=string<>")";
Return[string];
] *)

Ideal2String[Ideal1_,VarList1_]:=Module[{VList=VarList1,Ideal=Ideal1,CList,variables,str},

	CList=Complement[Variables[Ideal],VList];
	variables=VList/.variableNames[VList,CList];

	Ideal=Ideal/.variableNames[VList,CList];
	str=ToString[Poly2String[#,variables]&/@Ideal];

	Return[StringReplace[str,{"{"->"(","}"->")"}]];
]


Module2String[Module1_,VarList1_]:=Module[{VList=VarList1,Module=Module1,CList,var,str},

	CList=Complement[Variables[Module],VList];
	var=VList/.variableNames[VList,CList];

	Module=Module/.variableNames[VList,CList];
	str=ToString[Map[Poly2String[#,var]&,Module,{2}]];

	Return[str];
]


(*Functions to call Macaulay2*)

M2Initialization[VarList1_,CoefficientList1_,MonomialOrdering1_,Field1_]:=Module[{VList=VarList1,CList=CoefficientList1,MOrdering=MonomialOrdering1,Field=Field1,variables,coefficients},

str=OpenWrite[Datapath<>"/temp.m2"];
Run["rm "<>Datapath<>"/*.txt"];

variables=VList/.variableNames[VList,CList];
coefficients=CList/.variableNames[VList,CList];

If[CList=={},WriteString[str,"k="<>Field<>"\n"],
	{WriteString[str,"kk="<>Ring2String[coefficients,Field]<>"\n"];
	WriteString[str,"k=frac kk\n"];}
];

Switch[MOrdering,Lexicographic,ord="Lex",DegreeLexicographic,ord="GLex",DegreeReverseLexicographic,ord="GRevLex",_,ord=MOrdering];

WriteString[str,"R="<>StringReplace[Ring2String[variables,"k"],"]"->",MonomialOrder=>"<>ord]<>"]\n"];
Close[str];

]


(* Function to red the output of M2 *)

String2Ideal[str1_]:=Module[{string=str1,Ideal},

If[string=="ideal",Ideal={1},
string=StringReplace[string,"monomialideal("->"{"];
string=StringReplace[string,"ideal("->"{"];
string=StringReplacePart[string,"}",{StringLength[string],StringLength[string]}];
Ideal=ToExpression[string];
];
Return[Ideal];
];


(* ::Subsection::Closed:: *)
(*Macaulay2 Functions *)


Options[M2GroebnerBasis]={MonomialOrder->DegreeReverseLexicographic,GeneratingMatrix->False,NumberField->"QQ", OptionalInput->""};
M2GroebnerBasis[Object1_,VarList1_,OptionsPattern[]]:=Module[{Object=Object1,MatrixFlag=OptionValue[GeneratingMatrix],Field=OptionValue[NumberField],InputString=OptionValue[OptionalInput],VList=VarList1,CList,MOrdering=OptionValue[MonomialOrder],ord,GBasis,GMatrix,m,Ideal,Module},

CList=Complement[Variables[Object],VList];

M2Initialization[VList,CList,MOrdering,Field];
str=OpenAppend[Datapath<>"/temp.m2"];
If[InputString!="",InputString=","<>InputString];

Switch[ArrayDepth[Object],
	1,
	Ideal=Object;
	WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
	WriteString[str,"obj=promote(I,R)\n"];
	,2,
	Module=Object;
	WriteString[str,"n=matrix"<>Module2String[Module,VList]<>"\n"];
	WriteString[str,"obj=image n\n"];
];

Switch[MatrixFlag,
	True,
	WriteString[str,"G=gb(obj,ChangeMatrix=>true"<>InputString<>")\n
       g=gens G\n
       m=getChangeMatrix(G)\n
       \""<>Datapath<>"/Gbasis.txt\"<<toString g<<close\n
       \""<>Datapath<>"/Gmatrix.txt\"<<toString m<<close\n
       exit()\n"];
	,False,
	WriteString[str,"G=gb(obj"<>InputString<>")\n
       g=gens G\n
       \""<>Datapath<>"/Gbasis.txt\"<<toString g<<close\n
       exit()\n"];
];

Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];
str=OpenRead[Datapath<>"/Gbasis.txt"];
Read[str,Word];
GBasis=Flatten[ReadList[str],3-ArrayDepth[Object]];   (*    Flatten the Grobner basis by the order 2, if the input is an ideal, by the order 1, if the input is a module     *)
Close[str];

If[MatrixFlag==True,
	str=OpenRead[Datapath<>"/Gmatrix.txt"];
	Read[str,Word];
	GMatrix=Flatten[ReadList[str],1];
	Close[str];
];


Switch[MatrixFlag,True,Return[{GBasis,GMatrix}/.variableNamesBack[VList,CList]],False,Return[GBasis/.variableNamesBack[VList,CList]]];
]


Options[M2GroebnerBasis]={MonomialOrder->DegreeReverseLexicographic,GeneratingMatrix->False,NumberField->"QQ", OptionalInput->""};
M2GroebnerBasis[Object1_,VarList1_,OptionsPattern[]]:=Module[{Object=Object1,MatrixFlag=OptionValue[GeneratingMatrix],Field=OptionValue[NumberField],InputString=OptionValue[OptionalInput],VList=VarList1,CList,MOrdering=OptionValue[MonomialOrder],ord,GBasis,GMatrix,m,Ideal,Module},

CList=Complement[Variables[Object],VList];

M2Initialization[VList,CList,MOrdering,Field];
str=OpenAppend[Datapath<>"/temp.m2"];
If[InputString!="",InputString=","<>InputString];

Switch[ArrayDepth[Object],
	1,
	Ideal=Object;
	WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
	WriteString[str,"obj=promote(I,R)\n"];
	,2,
	Module=Object;
	WriteString[str,"n=matrix"<>Module2String[Module,VList]<>"\n"];
	WriteString[str,"obj=image n\n"];
];

Switch[MatrixFlag,
	True,
	WriteString[str,"G=gb(obj,ChangeMatrix=>true"<>InputString<>")\n
       g=gens G\n
       m=getChangeMatrix(G)\n
       \""<>Datapath<>"/Gbasis.txt\"<<toString g<<close\n
       \""<>Datapath<>"/Gmatrix.txt\"<<toString m<<close\n
       exit()\n"];
	,False,
	WriteString[str,"G=gb(obj"<>InputString<>")\n
       g=gens G\n
       \""<>Datapath<>"/Gbasis.txt\"<<toString g<<close\n
       exit()\n"];
];

Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];
str=OpenRead[Datapath<>"/Gbasis.txt"];
Read[str,Word];
GBasis=Flatten[ReadList[str],3-ArrayDepth[Object]];   (*    Flatten the Grobner basis by the order 2, if the input is an ideal, by the order 1, if the input is a module     *)
Close[str];

If[MatrixFlag==True,
	str=OpenRead[Datapath<>"/Gmatrix.txt"];
	Read[str,Word];
	GMatrix=Flatten[ReadList[str],1];
	Close[str];
];


Switch[MatrixFlag,True,Return[{GBasis,GMatrix}/.variableNamesBack[VList,CList]],False,Return[GBasis/.variableNamesBack[VList,CList]]];
]


Options[M2GBasis]={MonomialOrder->DegreeReverseLexicographic,NumberField->"ZZ/9001", OptionalInput->""};
M2GBasis[Ideal1_,VarList1_,OptionsPattern[]]:=Module[{Ideal=Ideal1,Field=OptionValue[NumberField],InputString=OptionValue[OptionalInput],VList=VarList1,CList,MOrdering=OptionValue[MonomialOrder],
ord,GBasis,m},

CList=Complement[Variables[Ideal],VList];

M2Initialization[VList,CList,MOrdering,Field];
str=OpenAppend[Datapath<>"/temp.m2"];
If[InputString!="",InputString=","<>InputString];


WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
WriteString[str,"I=promote(I,R)\n"];
WriteString[str,"M=groebnerBasis(I"<>InputString<>")\n
       \""<>Datapath<>"/Gbasis.txt\"<<toString M<<close\n
       exit()\n"];
Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];
str=OpenRead[Datapath<>"/Gbasis.txt"];
Read[str,Word];
GBasis=Flatten[ReadList[str],3-ArrayDepth[Ideal]];   (*    Flatten the Grobner basis by the order 2, if the input is an ideal, by the order 1, if the input is a module     *)
Close[str];


Return[GBasis/.variableNamesBack[VList,CList]];
]


Options[M2ArithmeticGenus]={MonomialOrder->DegreeLexicographic,NumberField->"QQ"};
M2ArithmeticGenus[Ideal1_,VarList1_,OptionsPattern[]]:=Module[{Ideal=Ideal1,VList=VarList1,CList,MOrdering=OptionValue[MonomialOrder],ord,genus,Field=OptionValue[NumberField]},

CList=Complement[Variables[Ideal],VList];

M2Initialization[VList,CList,MOrdering,Field];
str=OpenAppend[Datapath<>"/temp.m2"];
WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
WriteString[str,"I=promote(I,R)\n"];

WriteString[str,"VV=Proj(R/I);\n
       g=genus VV;\n
       \""<>Datapath<>"/genus.txt\"<<toString g<<close\n
       exit()\n"];
Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];
str=OpenRead[Datapath<>"/genus.txt"];
genus=Read[str,Word];
Close[str];
Return[genus//ToExpression];
]


Options[M2SingularLocus]={MonomialOrder->DegreeLexicographic,NumberField->"QQ",GBasis->True,Projective->True};
M2SingularLocus[Ideal1_,VarList1_,OptionsPattern[]]:=Module[{Ideal=Ideal1,VList=VarList1,CList,MOrdering=OptionValue[MonomialOrder],string,Field=OptionValue[NumberField],GBasisFlag=OptionValue[GBasis],ProjectiveFlag=OptionValue[Projective]},
CList=Complement[Variables[Ideal],VList];
M2Initialization[VList,CList,MOrdering,Field];


str=OpenAppend[Datapath<>"/temp.m2"];
WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
WriteString[str,"I=promote(I,R)\n"];
If[ProjectiveFlag==True,WriteString[str,"slocus=singularLocus Proj(R/I)\n"],WriteString[str,"slocus=singularLocus (R/I)\n"]];
WriteString[str,"
       \""<>Datapath<>"/singular.txt\"<<toString expression slocus<<close\n
       exit()\n"];
Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];
str=OpenRead[Datapath<>"/singular.txt"];

string=Read[str,Word];
string=StringReplace[string,"Proj("~~Longest[x___]~~")":>x];
If[string=="R/1",
		string="{1}",
		string=StringReplace[string,"R/("->"{"];
		string=StringReplacePart[string,"}",{StringLength[string],StringLength[string]}];
];
Ideal=ToExpression[string]/.variableNamesBack[VList,CList];
Close[str];
If[GBasisFlag==True,Ideal=M2GroebnerBasis[Ideal,VList,MonomialOrder->MonomialOrder,NumberField->Field]];
Return[Ideal];
]


Options[M2Degree]={MonomialOrder->DegreeLexicographic,NumberField->"QQ"};
M2Degree[Ideal1_,VarList1_,OptionsPattern[]]:=Module[{Ideal=Ideal1,VList=VarList1,CList,MOrdering=OptionValue[MonomialOrder],ord,deg,Field=OptionValue[NumberField]},

CList=Complement[Variables[Ideal],VList];
M2Initialization[VList,CList,MOrdering,Field];

str=OpenAppend[Datapath<>"/temp.m2"];
WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
WriteString[str,"I=promote(I,R)\n"];

WriteString[str,"
       deg=degree I;\n
       \""<>Datapath<>"/degree.txt\"<<toString deg<<close\n
       exit()\n"];
Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];
str=OpenRead[Datapath<>"/degree.txt"];
deg=Read[str,Word];
Close[str];
Return[deg//ToExpression];
]


Options[M2Dim]={MonomialOrder->DegreeLexicographic,NumberField->"QQ"};
M2Dim[Ideal1_,VarList1_,OptionsPattern[]]:=Module[{Ideal=Ideal1,VList=VarList1,CList,MOrdering=OptionValue[MonomialOrder],ord,dim,Field=OptionValue[NumberField]},

CList=Complement[Variables[Ideal],VList];
M2Initialization[VList,CList,MOrdering,Field];
str=OpenAppend[Datapath<>"/temp.m2"];
WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
WriteString[str,"I=promote(I,R)\n"];

WriteString[str,"
       d=dim I;\n
       \""<>Datapath<>"/dim.txt\"<<toString d<<close\n
       exit()\n"];
Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];
str=OpenRead[Datapath<>"/dim.txt"];
dim=Read[str,Word];
Close[str];
Return[dim//ToExpression];
]


Options[M2Radical]={MonomialOrder->DegreeLexicographic,NumberField->"QQ"};
M2Radical[Ideal1_,VarList1_,OptionsPattern[]]:=Module[{Ideal=Ideal1,VList=VarList1,radIdeal,CList,MOrdering=OptionValue[MonomialOrder],Field=OptionValue[NumberField],string},

	CList=Complement[Variables[Ideal],VList];
	M2Initialization[VList,CList,MOrdering,Field];
	str=OpenAppend[Datapath<>"/temp.m2"];
	WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
	WriteString[str,"I=promote(I,R)\n"];

	WriteString[str,"
       d=radical I;\n
       d=promote(d,R);\n
       \""<>Datapath<>"/radical.txt\"<<toString d<<close\n
       exit()\n"];
	Close[str];
	Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];
	str=OpenRead[Datapath<>"/radical.txt"];

	string=Read[str,Word];
	Close[str];	
	(* string=StringReplace[string,"monomialideal("->"{"];
	string=StringReplace[string,"ideal("->"{"];
	string=StringReplacePart[string,"}",{StringLength[string],StringLength[string]}];
	radIdeal=ToExpression[string]; *)

	radIdeal=String2Ideal[string]/.variableNamesBack[VList,CList];

	
	Return[radIdeal];
]


Options[M2PolynomialReduce]={MonomialOrder->DegreeLexicographic,NumberField->"QQ",QuotientComputation->True};
M2PolynomialReduce[f1_,Ideal1_,VarList1_,OptionsPattern[]]:=Module[{f=f1,Ideal=Ideal1,VList=VarList1,MOrdering=OptionValue[MonomialOrder],Field=OptionValue[NumberField],CList,QuotientFlag=OptionValue[QuotientComputation],quotient,r},

(********************************** Macaulay2 part **********************************)
CList=Complement[Variables[Ideal],VList];
M2Initialization[VList,CList,MOrdering,Field];
str=OpenAppend[Datapath<>"/temp.m2"];
WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
WriteString[str,"I=promote(I,R)\n"];
WriteString[str,"f="<>Poly2String[f/.variableNames[VList,CList],VList/.variableNames[VList,CList]]<>"\n"];
WriteString[str,"r=f% I\n"];
WriteString[str,"\""<>Datapath<>"/remainder.txt\"<<toString r<<close\n"];
If[QuotientFlag==True,{WriteString[str,"q=f//gens I\n"];
WriteString[str,"\""<>Datapath<>"/quotient.txt\"<<toString q<<close\n"];}];
WriteString[str,"exit()\n"];
Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];

(********************************** Macaulay2 part **********************************)
If[QuotientFlag==True,{str=OpenRead[Datapath<>"/quotient.txt"];
	Read[str,Word];
	quotient=Flatten[ReadList[str]];
	Close[str];}
];
str=OpenRead[Datapath<>"/remainder.txt"];
r=ToExpression[Read[str,Word]];
Close[str];
If[QuotientFlag==True,Return[{quotient,r}/.variableNamesBack[VList,CList]]];
Return[r/.variableNamesBack[VList,CList]];
]


Options[M2Intersect]={MonomialOrder->DegreeLexicographic,NumberField->"QQ"};
M2Intersect[Ideal1_,Ideal2_,VarList1_,OptionsPattern[]]:=Module[{I1=Ideal1,I2=Ideal2,VList=VarList1,CList,Ideal,MOrdering=OptionValue[MonomialOrder],Field=OptionValue[NumberField],string},

CList=Complement[Variables[Join[Ideal1,Ideal2]],VList];
M2Initialization[VList,CList,MOrdering,Field];
str=OpenAppend[Datapath<>"/temp.m2"];
WriteString[str,"I1=ideal"<>Ideal2String[I1,VList]<>"\n"];
WriteString[str,"I1=promote(I1,R)\n"];
WriteString[str,"I2=ideal"<>Ideal2String[I2,VList]<>"\n"];
WriteString[str,"I2=promote(I2,R)\n"];
WriteString[str,"I=intersect(I1,I2)\n"];
WriteString[str,"\""<>Datapath<>"/intIdeal.txt\"<<toString I<<close\nexit()\n"];
Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];
str=OpenRead[Datapath<>"/intIdeal.txt"];
string=Read[str,Word];
(*
string=StringReplace[string,"ideal("->"{"];
string=StringReplacePart[string,"}",{StringLength[string],StringLength[string]}]; *)


Ideal=String2Ideal[string]/.variableNamesBack[VList,CList];
Close[str];
(*Ideal=GroebnerBasis[Ideal,VList,MonomialOrder->Lexicographic,CoefficientDomain->RationalFunctions];*)
Return[Simplify[Ideal]];
]


Options[M2PrimaryDecomposition]={MonomialOrder->DegreeLexicographic,NumberField->"QQ"};
M2PrimaryDecomposition[Ideal1_,VarList1_,OptionsPattern[]]:=Module[{Ideal=Ideal1,VList=VarList1,string,CList={},MOrdering=OptionValue[MonomialOrder],Field=OptionValue[NumberField],result},M2Initialization[VList,{},MOrdering,Field];
str=OpenAppend[Datapath<>"/temp.m2"];
WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
WriteString[str,"I=promote(I,R)\n"];
WriteString[str,"ss=primaryDecomposition (I)
   \n"];
WriteString[str,"\""<>Datapath<>"/pDIdeal.txt\"<<toString ss<<close\nexit()\n"];
Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];
str=OpenRead[Datapath<>"/pDIdeal.txt"];
string=Read[str,String];
result=StringReplace[string,{"ideal("~~Shortest[x___]~~"), ":>"{"<>x<>"},","ideal("~~x___~~")}":>"{"<>x<>"}}"}];
result=ToExpression[result]/.variableNamesBack[VList,CList];
Return[result];
]





Options[M2Syzygy]={MonomialOrder->DegreeLexicographic, NumberField->"QQ", MinimalGenerators->False,OptionalInput->""};
M2Syzygy[object1_,VarList1_,OptionsPattern[]]:=Module[{object=object1,VList=VarList1,CList,MOrdering=OptionValue[MonomialOrder],InputString=OptionValue[OptionalInput],
MinimalFlag=OptionValue[MinimalGenerators],ord,Matrix,Ideal,Field=OptionValue[NumberField]},

If[InputString!="",InputString=","<>InputString];
CList=Complement[Variables[object],VList];
M2Initialization[VList,CList,MOrdering,Field];

str=OpenAppend[Datapath<>"/temp.m2"];

Switch[ArrayDepth[object],1,
		Ideal=object;
		WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
		WriteString[str,"I=promote(I,R)\n"];
		WriteString[str,"G=gb(I, Syzygies=>true"<>InputString<>")\n"];
		,
						2,
	Matrix=object;
	WriteString[str,"n=matrix"<>Module2String[Matrix,VList]<>"\n"];
	WriteString[str,"N=image n\n"];
	WriteString[str,"G=gb(N, Syzygies=>true"<>InputString<>")\n"];	
];

If[MinimalFlag==True,WriteString[str, "m=mingens image syz G\n"],WriteString[str, "m=syz G\n"]];
WriteString[str, " \""<>Datapath<>"/Gmatrix.txt\"<<toString m<<close\n  exit()\n"];

Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];

str=OpenRead[Datapath<>"/Gmatrix.txt"];
Read[str,Word];
Matrix=Flatten[ReadList[str],1];
Close[str];

Return[Matrix/.variableNamesBack[VList,CList]];
]




Options[M2Syz]={MonomialOrder->DegreeLexicographic, NumberField->"QQ", OptionalInput->""};
M2Syz[object1_,VarList1_,OptionsPattern[]]:=Module[{object=object1,VList=VarList1,CList,MOrdering=OptionValue[MonomialOrder],InputString=OptionValue[OptionalInput],
ord,Matrix,Ideal,Field=OptionValue[NumberField]},

If[InputString!="",InputString=","<>InputString];
CList=Complement[Variables[object],VList];
M2Initialization[VList,CList,MOrdering,Field];

str=OpenAppend[Datapath<>"/temp.m2"];

Switch[ArrayDepth[object],1,
		Ideal=object;
		WriteString[str,"I=ideal"<>Ideal2String[Ideal,VList]<>"\n"];
		WriteString[str,"I=promote(I,R)\n"];
		WriteString[str,"m=mingens image syz gens I\n"];
		,
						2,
	Matrix=object;
	WriteString[str,"n=matrix"<>Module2String[Matrix,VList]<>"\n"];
	(* WriteString[str,"N=image n\n"]; *)
	WriteString[str,"m=mingens image syz n\n"];	
];


WriteString[str, " \""<>Datapath<>"/Gmatrix.txt\"<<toString m<<close\n  exit()\n"];

Close[str];
Run[M2path<>" "<>Datapath<>"/temp.m2 --silent --stop"];

str=OpenRead[Datapath<>"/Gmatrix.txt"];
Read[str,Word];
Matrix=Flatten[ReadList[str],1];
Close[str];

Return[Matrix/.variableNamesBack[VList,CList]];
]


(* ::Subsection::Closed:: *)
(*Derivative Functions*)


Options[PrimaryDecomposition]={MonomialOrder->DegreeLexicographic,NumberField->"QQ",
AlgebraicExtension->{},PreDecomposition->False};
PrimaryDecomposition[Ideal1_,VarList1_,OptionsPattern[]]:=Module[{Ideal=Ideal1,VList=VarList1,flist={},string,n,CList,MOrdering=OptionValue[MonomialOrder],Field=OptionValue[NumberField],ExtensionEquations=OptionValue[AlgebraicExtension],PreFlag=OptionValue[PreDecomposition],ExtensionNumbers,IdealList,result={},i,j,Gr},

CList=Complement[Variables[Ideal],VList];

ExtensionNumbers=Variables[ExtensionEquations];
If[CList!={},Ideal=Table[Numerator[Together[Ideal[[i]]]],{i,1,Length[Ideal]}]];



Switch[PreFlag,False,
	IdealList=M2PrimaryDecomposition[Join[Ideal,ExtensionEquations],Join[VList,CList,ExtensionNumbers],NumberField->Field,MonomialOrder->MOrdering],
    _,
	n=Length[Ideal];
	IdealList={};
	flist=Table[Factorize[Ideal[[i]],Join[VList,ExtensionNumbers]],{i,1,n}];
    
	PreDecompositionList=Tuples[flist];
	
    If[PreFlag==0,Return[PreDecompositionList]];
    For[i=1,i<=Length[PreDecompositionList],i++,
		IdealList=Join[IdealList,M2PrimaryDecomposition[Join[PreDecompositionList[[i]],ExtensionEquations],Join[VList,CList,ExtensionNumbers],NumberField->Field,MonomialOrder->MOrdering]];
	];
];

result=IdealList;
If[CList!={},{
result={};
For[i=1,i<=Length[IdealList],i++,Gr=M2GroebnerBasis[IdealList[[i]],Join[VList,ExtensionNumbers],NumberField->Field];
If[Complement[Variables[Gr],CList]!={},result=Append[result,Gr]];];
If[result=={},result={{1}}];
}];
result=Table[Complement[result[[i]],ExtensionEquations],{i,1,Length[result]}];
Return[result];
]


Options[PolynomialDivision]={MonomialOrder->DegreeReverseLexicographic};
PolynomialDivision[Poly1_,Ideal1_,vlist1_,OptionsPattern[]]:=Module[{M,Poly=Poly1,Ideal=Ideal1,MOrder=OptionValue[MonomialOrder],G,clist,vlist=vlist1,q,r},
	clist=Complement[Variables[Ideal],vlist];
	{G,M}=M2GroebnerBasis[Ideal,vlist,MonomialOrder->MOrder,GeneratingMatrix->True];
	{q,r}=PolynomialReduce[Poly,G,vlist,MonomialOrder->MOrder,CoefficientDomain->RationalFunctions];
    q=M.q;
	Return[{q,r}];
];

Options[IntersectIdeal]={MonomialOrder->DegreeLexicographic,NumberField->"QQ"};
IntersectIdeal[IdealList1_,VarList1_,OptionsPattern[]]:=Module[{IdealList=IdealList1,VList=VarList1,Ideal,i,len,MOrdering=OptionValue[MonomialOrder],Field=OptionValue[NumberField]},len=Length[IdealList];
Ideal=IdealList[[1]];
For[i=1,i<=len-1,i++,
		Ideal=M2Intersect[Ideal,IdealList[[i+1]],VList,NumberField->Field,MonomialOrder->MOrdering];
		
	(* Print["Step ",i," is finished"];  *)



		Ideal=GroebnerBasis[Ideal,VList,MonomialOrder->MOrdering,CoefficientDomain->RationalFunctions]; 


];
Return[Ideal];]




Elimination[Ideal1_, VList1_, EVList1_] := 
 Module[{Ideal = Ideal1, VList = VList1, EVList = EVList1, RVList, Gr},

  (* EVList is the list for vairables to be eliminated. *)
	(*  RVList = Complement[VList, EVList]; *)

	RVList=Select[VList,!MemberQ[EVList,#]&];
	
(* Print[Join[EVList, RVList]]; *)
  Gr = GroebnerBasis[Ideal, Join[EVList, RVList], MonomialOrder -> Lexicographic, CoefficientDomain -> RationalFunctions];
  Return[Select[Gr,Intersection[Variables[#],EVList]=={}&]];
 ]


Implicitization[rationalFunctions_, parameters_, variables_] := 
 Module[{Target = rationalFunctions, pList = parameters, 
   VList = variables, len, i, f, g, gg, variableY, Ideal, result},
  len = Length[Target];
  If[len != Length[VList], Return["The sizes are not consistent"];];
  Target = Table[Together[Target[[i]]], {i, 1, len}];
  f = Table[Numerator[Target[[i]]], {i, 1, len}];
  g = Table[Denominator[Target[[i]]], {i, 1, len}];
  gg = Product[g[[i]], {i, 1, len}];
  
  Ideal = Table[VList[[i]]*g[[i]] - f[[i]], {i, 1, len}];
  Ideal = Append[Ideal, 1 - gg *variableY];
  
  result = 
   Elimination[Ideal, Join[pList, {variableY}, VList], 
    Join[{variableY}, pList]]; Return[result];
  ]


CompareIdeal[Ideal1_,Ideal2_,vlist1_]:=Module[{I1=Ideal1,I2=Ideal2,vlist=vlist1,G1,G2,ratio},
	G1=GroebnerBasis[I1,vlist,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions];
	G2=GroebnerBasis[I2,vlist,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions];
	If[Length[G1]!=Length[G2],Return[False]];
	G1=LeadingTermUnit[G1,vlist,DegreeLexicographic];
	G2=LeadingTermUnit[G2,vlist,DegreeLexicographic];
	If[Complement[G1,G2]=={}&&Complement[G2,G1]=={},Return[True]];
	Return[False];
];


SubIdeal[Ideal1_,Ideal2_,vlist1_]:=Module[{I1=Ideal1,I2=Ideal2,vlist=vlist1,G2,i,result},
	
	G2=GroebnerBasis[I2,vlist,MonomialOrder->DegreeReverseLexicographic,CoefficientDomain->RationalFunctions];

	result={};
	For[i=1,i<=Length[I1],i++,
		AppendTo[result,PolynomialReduce[I1[[i]],G2,vlist,MonomialOrder->DegreeReverseLexicographic,CoefficientDomain->RationalFunctions][[2]]];
	];

	Return[result];
	

];


Options[FractionReduce]={MonomialOrder->DegreeLexicographic,NumberField->"QQ",CombineTerms->False};
FractionReduce[fraction1_,ideal1_,VarList1_,OptionsPattern[]]:=Module[{VList=VarList1,MOrdering=OptionValue[MonomialOrder],Field=OptionValue[NumberField],CombineFlag=OptionValue[CombineTerms],ideal=ideal1,len,list,M,gr,r,phi,num,den,fraction=fraction1},fraction=Together[fraction];
num=Numerator[fraction];
den=Denominator[fraction];
len=Length[ideal];
{gr,M}=M2GroebnerBasis[Join[ideal,{den}],VList,MonomialOrder->MOrdering,NumberField->Field,GeneratingMatrix->True];
(* Print[gr,M]; *)
{list,r}=PolynomialReduce[num,gr,VList,CoefficientDomain->RationalFunctions,MonomialOrder->MOrdering];
r=r//Simplify;
phi=(M.list)[[len+1]];
If[CombineFlag==True,Return[phi+r/den]];
Return[{phi,r/den}];]






Options[recursiveReduce]={MonomialOrder->DegreeReverseLexicographic,CompleteReduction->True,MaximumOrder->Infinity,Timer->False};
recursiveReduce[f1_,divisor1_,var1_,OptionsPattern[]]:=Module[{nullList,f=f1,den=divisor1,GrMatrix,n,i,var=var1,Ideal,Gr,r,result={},qlist,MOrder=OptionValue[MonomialOrder],CFlag=OptionValue[CompleteReduction]
,Max=OptionValue[MaximumOrder],timeFlag=OptionValue[Timer],timer},

n=Length[den];
nullList[i_]:=If[CFlag==True,Table[0,{j,1,Binomial[n,i]}],0];


If[timeFlag==True,timer=AbsoluteTime[]];
If[Max<n,For[i=n,i>Max,i--,AppendTo[result,nullList[i]]]];

For[i=Min[Max,n],i>=1,i--,
Ideal=Times@@@Subsets[den,{i}];
If[f==0,AppendTo[result,nullList[i]]; Continue[]];


	If[CFlag==True,
		{Gr,GrMatrix}=M2GroebnerBasis[Ideal,var,MonomialOrder->MOrder,GeneratingMatrix->True];
		{qlist,r}=PolynomialReduce[f,Gr,var,MonomialOrder->MOrder,CoefficientDomain->RationalFunctions];
		AppendTo[result,GrMatrix.qlist];
,			
		Gr=GroebnerBasis[Ideal,var,MonomialOrder->MOrder];
		r=PolynomialReduce[f,Gr,var,MonomialOrder->MOrder,CoefficientDomain->RationalFunctions][[2]];
		AppendTo[result,f-r];
	];
(* To simplify the reminder *)
f=r//Expand;  
If[timeFlag==True,Print["Order ",i," finished     ",AbsoluteTime[]-timer]];
];

If[CFlag==True,AppendTo[result,{f}],AppendTo[result,f]];
Return[result];
];


Options[FullDivision]={ExcludedDivisors->{},Timing->False};
FullDivision[f1_,Den1_,var1_,OptionsPattern[]]:=Module[{f=f1,Den=Den1,var=var1,LocalDen,IncludedIndex,ExcludedIndex=OptionValue[ExcludedDivisors],n,r,localn,qlist,ExcludedDivisorsProd,j,
TimeFlag=OptionValue[Timing],timer,Gr,Matrix,q,cachedFlag=False,clist
},
	
If[f==0,Return[0]];

n=Length[Den];
localn=n-Length[ExcludedIndex];
If[localn==0,Return[f]];

If[localn==n,Clear[Global`FullDivisionGR,Global`FullDivisionMatrix]]; (* Initialization *)

ExcludedDivisorsProd=Times@@Global`de/@ExcludedIndex;
IncludedIndex=Complement[Table[i,{i,1,n}],ExcludedIndex];
LocalDen=Den[[IncludedIndex]];

If[TimeFlag==True,timer=AbsoluteTime[]];
If[ListQ[Global`FullDivisionGR[IncludedIndex]]==True,
	cachedFlag=True;
	Gr=Global`FullDivisionGR[IncludedIndex];
	Matrix=Global`FullDivisionMatrix[IncludedIndex];
,
	
	{Gr,Matrix}=M2GroebnerBasis[LocalDen,var,MonomialOrder->DegreeReverseLexicographic,GeneratingMatrix->True];
	Global`FullDivisionGR[IncludedIndex]=Gr;
	Global`FullDivisionMatrix[IncludedIndex]=Matrix;
];
{q,r}=PolynomialReduce[f,Gr,var,MonomialOrder->DegreeReverseLexicographic,CoefficientDomain->RationalFunctions];
qlist=Matrix.q;

If[TimeFlag==True,
	(* Print["quotients: ",qlist]; *)
	(* Print["remainder: ",r]; *)
	Print[IncludedIndex,"   ",AbsoluteTime[]-timer,If[cachedFlag==True,"   using cached data",""]];
];

Return[Sum[j=IncludedIndex[[i]];
FullDivision[qlist[[i]],Den,var,Timing->TimeFlag,ExcludedDivisors->Sort[Append[ExcludedIndex,j]]]*Global`de[j],{i,1,localn}]+r];
];


(* ::Subsection::Closed:: *)
(*Congruence over a polynomial ring*)


CRTprepare[Ideal1_,Ideal2_,Var1_]:=Module[{result,M1,M2,I1=Ideal1,I2=Ideal2,Var=Var1,len1,len2,I1p2,CList,GB,GMatrix},
	I1p2=Join[I1,I2];
	
	len1=Length[I1];
	len2=Length[I2];

	{GB,GMatrix}=M2GroebnerBasis[I1p2,Var,MonomialOrder->DegreeLexicographic,GeneratingMatrix->True];
	
	
	M1=Table[GMatrix[[i]],{i,1,len1}];
	M2=Table[GMatrix[[i]],{i,len1+1,len1+len2}];

	result={I1.M1,I2.M2}//Factor;

	Return[Append[result,GB]];

];


Options[quickCRT]={QuietMode->True};
quickCRT[F1_,F2_,VarList1_,basis1_,intIdeal1_,OptionsPattern[]]:=Module[{f1=F1,f2=F2,Var=VarList1,basis=basis1,intIdeal=intIdeal1,q,r,g1,g2,result,QFlag=OptionValue[QuietMode]},
	{q,r}=PolynomialReduce[f1-f2,basis[[3]],Var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions];
	g2=q.basis[[2]]//Factor;

	
	result=PolynomialReduce[f2+g2,intIdeal,Var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[2]];

	If[QFlag==False,Print[r//Simplify]];
	Return[result];
];


Options[CRT2]={MonomialOrder->DegreeLexicographic,NumberField->"QQ"};
CRT2[Set1_,Set2_,VarList1_,OptionsPattern[]]:=Module[{S1=Set1,S2=Set2,VList=VarList1,g,Flag=1,GB,GMatrix,I1p2,f1,f2,I1,I2,len1,len2,gg,r,i,j,Table1,F,I1c2,MOrdering=OptionValue[MonomialOrder],Field=OptionValue[NumberField]},{f1,I1}=S1;
{f2,I2}=S2;
If[Simplify[f1-f2]==0,F=f1; Print["checkpoint: ",0]; Flag=0];
If[Flag==1,
	len1=Length[I1];
	len2=Length[I2];
	I1p2=Join[I1,I2];
	{GB,GMatrix}=M2GroebnerBasis[I1p2,VList,MonomialOrder->MOrdering,NumberField->Field,GeneratingMatrix->True];
	{gg,r}=M2PolynomialReduce[f1-f2,GB,VList,NumberField->Field,MonomialOrder->MOrdering];
	Print["checkpoint: ",r];
	Table1=Table[Sum[I1p2[[i]] GMatrix[[i,j]],{i,1,len1}],{j,1,Length[gg]}];
	F=f1-Table1.gg//Simplify;
];
I1c2=M2Intersect[I1,I2,VList,MonomialOrder->MOrdering,NumberField->Field];
g=M2GroebnerBasis[I1c2,VList,MonomialOrder->MOrdering,NumberField->Field];
F=M2PolynomialReduce[F,g,VList,NumberField->Field,MonomialOrder->MOrdering][[2]];
Return[{F,I1c2}];]


Options[CRT]={MonomialOrder->DegreeLexicographic,NumberField->"QQ"};
CRT[fList1_,idealList1_,VarList1_,OptionsPattern[]]:=Module[{VList=VarList1,MOrdering=OptionValue[MonomialOrder],Field=OptionValue[NumberField],fList=fList1,idealList=idealList1,len,ideal,f,i},len=Length[fList];
Print[len];
ideal=idealList[[1]];
f=fList[[1]];
For[i=1,i<=len-1,i++,{f,ideal}=CRT2[{f,ideal},{fList[[i+1]],idealList[[i+1]]},VList,MonomialOrder->MOrdering,NumberField->Field];];
Return[f];]


(* ::Subsection::Closed:: *)
(*Factorization of one polynomial over C*)


Options[FindFactor]={Monitoring->False};
FindFactor[poly1_,vlist1_,OptionsPattern[]]:=Module[{poly=poly1,vlist=vlist1,len,deg,i,LT,qlist,clist,q,quo,r,rlist,ss,k,FL,polylist={},replacement,MonitorFlag=OptionValue[Monitoring],result,totalDeg,otherfactor},

result={1,poly};
totalDeg=PolynomialDegree[poly,vlist];
If[totalDeg<=1,Return[result]];

FL=FactorList[poly];

For[i=1,i<=Length[FL],i++,
If[PolynomialDegree[FL[[i,1]],vlist]>0,
polylist=Join[polylist,Table[FL[[i,1]],{j,1,FL[[i,2]]}]],
polylist=Append[polylist,FL[[i,1]]^FL[[i,2]]]
];
];


polylist=Select[polylist,Switch[#,1,False,_,True]&];



replacement=Table[vlist[[i]]->1,{i,1,Length[vlist]}];

For[k=1,k<=Length[polylist],k++,
	otherfactor=1;
	For[i=1,i<=Length[polylist],i++,
		If[i!=k,otherfactor*=polylist[[i]]];
		];
	deg=PolynomialDegree[polylist[[k]],vlist];
	If[deg==0,Continue[]];
	If[deg==1,result={polylist[[k]],otherfactor}; Break[]];

	LT=MonomialList[polylist[[k]],vlist,DegreeLexicographic][[1]];
	TargetList=MonomialList[(Total[vlist]+1)^(deg-1),vlist,DegreeLexicographic];

	TargetList=TargetList/(TargetList/.replacement);
	len=Length[TargetList];
	For[i=len-1,i>=1,i--,
		If[MonitorFlag==True,Print[i," sectors to go..."]];
		If [PolynomialReduce[LT,{TargetList[[i]]},vlist][[2]]==0,
			qlist=Table[TargetList[[j]],{j,i,len}];

			clist=Table[cc[j],{j,1,Length[qlist]}];
			clist[[1]]=1;
			q=clist.qlist;
			{quo,r}=PolynomialReduce[polylist[[k]],q,vlist,MonomialOrder->DegreeLexicographic];
			rlist=MonomialList[r,vlist,DegreeLexicographic]/.replacement;

			ss=ListSolve[rlist,Complement[clist,{1}]];
			If[ss!={},Break[]];
		];
	];
	If[i>0,result={q/.ss[[1]],(quo[[1]]/.ss[[1]])*otherfactor}; Break[];];
	If[deg<totalDeg,result={polylist[[k]],otherfactor}; Break[]];
];
Return[result];
]


Factorize[poly1_,vlist1_]:=Module[{poly=poly1,vlist=vlist1,result={},F,p,q},
F =poly;
{p,q}=FindFactor[F,vlist];
While[PolynomialDegree[p,vlist]!=0,
result=Append[result,p];
F=q;
{p,q}=FindFactor[F,vlist];
];
result=Append[result,F];
Return[result];
]




(* ::Subsection::Closed:: *)
(*Singular point analysis*)


Options[TangentCone]={Projective->False};
TangentCone[Ideal1_,Var1_,point1_,OptionsPattern[]]:=Module[{len,homePoly,Poly,t,Ideal=Ideal1,Var=Var1,point=point1,TC={},i,j,n,k,ProjectiveFlag=OptionValue[Projective]},
    n=Length[Var];   
	If[ProjectiveFlag==True,
					For[i=1,i<=n,i++,If[Simplify[point[[i]]!=0],k=i; Break[]];];
					If[i>n,Return[0]];
					Ideal=Ideal/.Var[[k]]->1;
					point=point/point[[k]]//Simplify;
					point=Delete[point,k];
					Var=Delete[Var,k];
					n--;
		];
	Ideal=Ideal/.Table[Var[[i]]->Var[[i]]+point[[i]],{i,1,n}]//Simplify;
	len=Length[Ideal];
	For[i=1,i<=len,i++,
		Poly=Ideal[[i]];
		Poly=Poly/.Table[Var[[i]]->Var[[i]]*t,{i,1,n}];
		j=0;
        homePoly=SeriesCoefficient[Poly,{t,0,j}];
		While[Simplify[homePoly]==0,
			j++;
			homePoly=SeriesCoefficient[Poly,{t,0,j}];
		];
		AppendTo[TC,homePoly/.t->1];
	];
	Return[TC];
];


Options[SingularPoints]={PlaneCurve->False};
SingularPoints[pIdeal1_,Var1_,OptionsPattern[]]:=Module[{pIdeal=pIdeal1,Var=Var1,Sing,ss,ss1,ss2,len,i,pointlist,TC,result={},PlanarFlag=OptionValue[PlaneCurve]},
	If[PlanarFlag==False, 
			Sing=M2SingularLocus[pIdeal,Var];		
		,
			Sing=Table[D[pIdeal[[1]],Var[[i]]],{i,1,Length[Var]}];
	];

	(* ss=ListSolve[Join[pIdeal,Sing],Var]; *)
	ss1=ListSolve[Join[pIdeal,Sing]/.Var[[1]]->1,Delete[Var,1]];
	pointlist=Table[Var//.ss1[[i]]/.Var[[1]]->1,{i,1,Length[ss1]}];
	ss2=ListSolve[Join[pIdeal,Sing]/.Var[[1]]->0,Delete[Var,1]];
	
	pointlist=Join[pointlist,Table[Var//.ss2[[i]]/.Var[[1]]->0,{i,1,Length[ss2]}]];

	pointlist=Complement[pointlist,{Table[0,{i,1,Length[Var]}]}];
	pointlist=pointlist/.Table[Var[[i]]->1,{i,1,Length[Var]}];
	len=Length[pointlist];

	Return[pointlist];

];


Options[CurveTangentLine]={};
CurveTangentLine[Eqn1_,Var1_,Value1_,OptionsPattern[]]:=Module[{lines,Eqn=Eqn1,Var=Var1,Value=Value1,Rules,len,ord,target,a,result},
len=Length[Value];
Rules=Table[Var[[i]]->Var[[i]]+Value[[i]],{i,1,len}];
(* Eqn=N[Eqn/.Rules,100];  *)

Eqn=Eqn/.Rules;
Rules=Table[Var[[i]]->a Var[[i]],{i,1,len}];
Eqn=Eqn/.Rules;
ord=0;
While[FullSimplify[Chop[SeriesCoefficient[Eqn,{a,0,ord}]]==0],
ord++;
];
result=FullSimplify[Chop[SeriesCoefficient[Eqn,{a,0,ord}]]];

lines=NSolve[(result/.Var[[1]]->1)==0,Var[[2]],WorkingPrecision->50];

Return[{ord,Length[Union[lines]],lines//N}];
];


Options[CurveAnalysis]={WorkingPrecision->20};
CurveAnalysis[planeCurve1_,Var1_,NewVariable1_,OptionsPattern[]]:=Module[{F=planeCurve1,Precision=OptionValue[WorkingPrecision],Var=Var1,F1,F2,G,G1,G2,G3,x,y,t=NewVariable1,AffineSolution,i,InfiniteSolution1,InfiniteSolution2},
{x,y}=Var;
F1=D[F,x];
F2=D[F,y];
AffineSolution=NSolve[{F,F1,F2}=={0,0,0},{x,y},WorkingPrecision->Precision]//Union;
Print[Length[AffineSolution]," singular points in the affine plane..."];

For[i=1,i<=Length[AffineSolution],i++,
Print[AffineSolution[[i]]//N];
Print[CurveTangentLine[F,{x,y},{x,y}/.AffineSolution[[i]]]];
Print["**************************"];
];

{G}=ProjectiveIdeal[{F},{x,y},t];
G1=D[G,x];
G2=D[G,y];
G3=D[G,t];
InfiniteSolution1=NSolve[({G,G1,G2,G3}/.t->0/.y->1)=={0,0,0,0},{x},WorkingPrecision->Precision]//Union;
InfiniteSolution2={{x->1,y->0,t->0}};
If[FullSimplify[{G,G1,G2,G3}/.InfiniteSolution2[[1]]]!={0,0,0,0},InfiniteSolution2={}];
Print[Length[InfiniteSolution1]+Length[InfiniteSolution2]," singular points at the infinite..."];
For[i=1,i<=Length[InfiniteSolution1],i++,
Print[Join[InfiniteSolution1[[i]],{y->1,t->0}]];
Print[CurveTangentLine[G/.y->1,{x,t},{x,t}/.InfiniteSolution1[[i]]]];
Print["**************************"];
];
Print["&&&&&&&&"];
For[i=1,i<=Length[InfiniteSolution2],i++,
Print[InfiniteSolution2[[i]]];
Print[CurveTangentLine[G/.x->1,{y,t},{y,t}/.InfiniteSolution2[[i]]]];
Print["**************************"];
];
Return[G];
];



(* ::Subsection::Closed:: *)
(*Multivariate Residue *)


PolynomialFactor[poly1_,varlist_]:=Module[{poly=poly1,vlist=varlist,list,i,result={}},
list=FactorList[poly];
For[i=1,i<=Length[list],i++,
If[Intersection[Variables[list[[i,1]]],vlist]!={},
AppendTo[result,list[[i]]];
];
];
Return[result];
];


reduceFactor[poly1_,varlist_]:=Module[{poly=poly1,vlist=varlist,n,list},
list=PolynomialFactor[poly,vlist];
n=Length[list];

Return[Product[list[[i,1]]^list[[i,2]],{i,1,n}]];
];


PolynomialOffset[target1_,ref1_,varlist_]:=
Module[{target=target1,ref=ref1,vlist=varlist,i,j,result,list1,list2,powerlist},
list1=PolynomialFactor[target,vlist];
list2=PolynomialFactor[ref,vlist];
powerlist=Table[0,{i,1,Length[list2]}];

For[i=1,i<=Length[list2],i++,
For[j=1,j<=Length[list1],j++,
If[list2[[i,1]]==list1[[j,1]]&&list1[[j,2]]-list2[[i,2]]>0,
powerlist[[i]]=(list1[[j,2]]-list2[[i,2]]);
Break[];];
];
];

Return[Product[list2[[i,1]]^powerlist[[i]],{i,1,Length[list2]}]];

];


ResidueLocus[list1_,vlist1_]:=Module[{fList=list1,vlist=vlist1,len,fProd,residueLocuslist,F1,F2,ss,k,i,j},

len=fList//Length;
fProd=Product[fList[[i]],{i,1,len}];
residueLocuslist={};
For[i=1,i<=len,i++,
For[j=i+1,j<=len,j++,
F1=fList[[i]];
F2=fList[[j]];
ss=Solve[{F1,F2}=={0,0},vlist];
For[k=1,k<=Length[ss],k++,
AppendTo[residueLocuslist,{vlist/.ss[[k]],{F1,F2}}];
];
];
];
Return[residueLocuslist//Sort];
];


ShiftList[list_]:=Append[Table[list[[i]],{i,2,Length[list]}],list[[1]]];


Transformation[fList1_,varlist1_]:=Module[{fList=fList1,varlist=varlist1,n,i,j,var,result,Matrix,Q,M,coefficientList},
n=Length[fList];
var=varlist;

result={};
Matrix={};

For[i=1,i<=n,i++,
var=ShiftList[var];

{Q,M}=M2GroebnerBasis[fList,var,GeneratingMatrix->True,MonomialOrder->Lexicographic];
AppendTo[result,Q[[1]]];
AppendTo[Matrix,Transpose[M][[1]]];
];
Return[{result,Matrix}];
]; 


Options[Transformation2]:={ParallelMode->True};
Transformation2[fList1_,varlist1_,OptionsPattern[]]:=Module[{fList=fList1,varlist=varlist1,m,n,i,var,PermutedVarList,Matrix,QList,coefficientList,Gr,A,B,vlist,ParallelMode1=OptionValue[ParallelMode]},
n=Length[fList];
coefficientList=Complement[Variables[fList],varlist];
{Gr,A}=M2GroebnerBasis[fList,varlist,MonomialOrder->DegreeLexicographic,GeneratingMatrix->True];

var=varlist;
PermutedVarList={};
For[i=1,i<=n,i++,
var=ShiftList[var];
AppendTo[PermutedVarList,var];
];

If[ParallelMode1==True,
	{
	DistributeDefinitions[PermutedVarList,fList,n];
	QList=ParallelTable[GroebnerBasis[fList,PermutedVarList[[i]],MonomialOrder->Lexicographic,CoefficientDomain->RationalFunctions][[1]],{i,1,n}];
	DistributeDefinitions[QList,Gr,varlist,n];
	B=ParallelTable[PolynomialReduce[QList[[i]],Gr,varlist,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[1]]//Simplify,{i,1,n}];
	},
	{
	QList=Table[GroebnerBasis[fList,PermutedVarList[[i]],MonomialOrder->Lexicographic,CoefficientDomain->RationalFunctions][[1]],{i,1,n}];
	B=Table[PolynomialReduce[QList[[i]],Gr,varlist,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[1]],{i,1,n}];
	}
];

B=Transpose[B];
(* m=Length[B]; 
 Grh=GroebnerBasis[result,varlist,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions];
A=Table[PolynomialReduce[A[[i,j]],Gr,varlist,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[2]],{i,1,n},{j,1,m}]; 
B=Table[PolynomialReduce[B[[i,j]],Gr,varlist,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[2]],{i,1,m},{j,1,n}]; 
*)

Matrix=A.B; 
Matrix=Transpose[Matrix];

If[ParallelMode1==True,
		{DistributeDefinitions[Matrix,n];
		 Matrix=ParallelTable[Factor[Matrix[[i,j]]],{i,1,n},{j,1,n},Method->"FinestGrained"];
		},
		Matrix=Simplify[Matrix];
];




Return[{QList,Matrix}];
];


PoleDegree[exp1_,list_]:=Residue[D[exp1,list[[1]]]/exp1,list];


Options[UnivariateResidue]={Algorithm->"Differential"};
UnivariateResidue[H1_,fList1_,varlist1_,locus1_,OptionsPattern[]]:=Module[{H=H1,fList=fList1,varlist=varlist1,locus=locus1,result,i,Alg=OptionValue[Algorithm],powerList,target,resiude},
	


If[Alg=="SeriesCoefficient",
	result=H/Product[fList[[i]],{i,1,Length[fList]}];
	For[i=1,i<=Length[varlist],i++,result=SeriesCoefficient[result,{varlist[[i]],locus[[i]],-1}];];
];
If[Alg=="Differential",
	powerList=Table[PoleDegree[1/fList[[i]],{varlist[[i]],locus[[i]]}],{i,1,Length[fList]}];

	target=H;
	
	For[i=1,i<=Length[varlist],i++,
		target=target/FullSimplify[fList[[i]]/(varlist[[i]]-locus[[i]])^-powerList[[i]]];
		residue=D[target,{varlist[[i]],-powerList[[i]]-1}]/Factorial[-powerList[[i]]-1];
		residue=residue/.{varlist[[i]]->locus[[i]]};
		target=residue;
];

	result=target;

];

Return[result];
];


RegularResidue[H1_,fList1_,varlist1_,locus1_]:=Module[{H=H1,fList=fList1,varlist=varlist1,locus=locus1,result,J,i,numeric},
numeric=Table[varlist[[i]]->locus[[i]],{i,1,Length[varlist]}];
If[Simplify[(fList/.numeric)==Table[0,{i,1,Length[varlist]}]],



J=Det[D[fList,{varlist}]];

J=Simplify[J];

J=Simplify[J/.numeric];
If[J==0,Return["SinguluarFlag"]];

Return[H/J/.Table[varlist[[i]]->locus[[i]],{i,1,Length[varlist]}]];
];
Return[0];
];


Options[MultiResidue]={Algorithm->2,ParallelMode->True,QuietMode->True};
MultiResidue[H1_,fList1_,varlist1_,locus1_,OptionsPattern[]]:=
Module[{H=H1,fList=fList1,varlist=varlist1,locus=locus1,result,qList,M,target,n,AlgorithmFlag=OptionValue[Algorithm],ParallelMode1=OptionValue[ParallelMode],QuietMode1=OptionValue[QuietMode],
num,den,numList,targetList,timer},
If[QuietMode1!=True,timer=AbsoluteTime[]];
n=Length[varlist];
result=RegularResidue[H,fList,varlist,locus];
If[result=="SinguluarFlag",
	Switch[AlgorithmFlag,1,{qList,M}=Transformation[fList,varlist],2,{qList,M}=Transformation2[fList,varlist,ParallelMode->ParallelMode1]];
	
	If[QuietMode1!=True,Print["Matrix found...  ",AbsoluteTime[]-timer,"    ",M//ByteCount]]; 

	target=H*Det[M];

		
	If[QuietMode1!=True, Print["Determinant found...  ",AbsoluteTime[]-timer,"     ",target//ByteCount]]; 
(*		
	target=Together[target];	 
	num=Numerator[target];

	Print["num found",SessionTime[],"     ",num//ByteCount]; 
	num=Factor[num];

		 Print["num simplified",SessionTime[],"     ",num//ByteCount]; 

	den=Denominator[target]; 
	
	If[Intersection[Variables[den],varlist]=={},
			result=UnivariateResidue[num,qList,varlist,locus]/den;
		,
			result=UnivariateResidue[num/den,qList,varlist,locus];
		];
 
*)

	 target=Factor[target];	 

	If[QuietMode1!=True,Print["Determinant simplified...  ",AbsoluteTime[]-timer ,"     ",target//ByteCount]]; 
	result=Simplify[UnivariateResidue[target,qList,varlist,locus]]; 
	If[QuietMode1!=True,Print["Time used...  ",AbsoluteTime[]-timer]; Print[];]; 
	];
Return[result];
];


residue[exp1_,fList1_,varlist1_,locus1_]:=Module[{exp=exp1,fList=fList1,varlist=varlist1,locus=locus1,result,num,den,n,offsetList,h},

n=Length[varlist];
exp=Together[exp];
num=Numerator[exp];
den=Denominator[exp];

offsetList=Table[PolynomialOffset[den,fList[[i]],varlist],{i,1,n}];

fList=Table[fList[[i]]*offsetList[[i]],{i,1,n}];


h=num/(den)*Product[fList[[i]],{i,1,n}]//Simplify;


result=MultiResidue[h,fList,varlist,locus];

Return[result//Simplify];
];



Bezoutian[flist_,varlist_,varlist1_]:=Module[{fList=flist,varList=varlist,n,m,varList1=varlist1,y,i,j,Matrix,replacement},
		n=Length[varList];
		m=Length[fList];
		For[i=0,i<=n,i++,
			replacement[i]=Table[If[j<=i,varList[[j]]->varList1[[j]],varList[[j]]->varList[[j]]],{j,1,n}];
		];
		Matrix=Table[((fList[[i]]/.replacement[j-1])-(fList[[i]]/.replacement[j]))/(varList[[j]]-varList1[[j]]),{i,1,m},{j,1,n}];
		
		Return[Matrix//Factor];

];


Options[GlobalDual]={QuietMode->True,SolvingMethod->"GroebnerBasisNum"};
GlobalDual[flist_,varlist_,OptionsPattern[]]:=Module[{target,clist,cc,Eqns,muList,len,A,IA,r,fList=flist,varList=varlist,varList1,n,m,y,i,j,f,Bmatrix,Gr,Gr1,forwardRep,backwardRep,basis,XXX,dualBasis,Trivalization,ttt,quietFlag=OptionValue[QuietMode],sMethod=OptionValue[SolvingMethod]},

		ttt=SessionTime[];
		
		n=Length[varList];
		varList1=Table[y[i],{i,1,n}];

		forwardRep=Table[varList[[i]]->varList1[[i]],{i,1,n}];
		backwardRep=Table[varList1[[i]]->varList[[i]],{i,1,n}];
		Trivalization=Table[varList[[i]]->1,{i,1,n}];

		basis=QuotientRingBasis[fList,varList];
		len=Length[basis];
		Bmatrix=Bezoutian[fList,varList,varList1];
		f=Det[Bmatrix]//Factor;

		
		If[quietFlag==False,Print["Bezoutian found,    ",SessionTime[]-ttt];ttt=SessionTime[];];
	
		Gr=GroebnerBasis[fList,varList,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions];
		Gr1=Gr/.forwardRep;
		
		f=PolynomialReduce[f,Join[Gr,Gr1],Join[varList,varList1],MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[2]];

		If[quietFlag==False,Print["f found,    ",SessionTime[]-ttt];ttt=SessionTime[];];
		
		dualBasis=MonomialCoefficients[f,varList,Total[basis],DegreeLexicographic];

		dualBasis=dualBasis/.backwardRep;
	
		dualBasis=dualBasis//Factor;

		If[quietFlag==False,Print["Dual basis found,    ",SessionTime[]-ttt];ttt=SessionTime[];];
		
	    A={};
		For[i=1,i<=len,i++,
			r=PolynomialReduce[dualBasis[[i]],Gr,varList,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[2]];
			AppendTo[A,MonomialCoefficients[r,varList,Total[basis],DegreeLexicographic]];
		];

		A=A//Simplify;

		If[quietFlag==False,Print["Converting matrix found,    ",SessionTime[]-ttt];ttt=SessionTime[];];

		

		target=Table[0,{i,1,len}];
		target[[len]]=1;

		Switch[sMethod,
			"LinearSolve",muList=LinearSolve[Transpose[A],target],
			"GroebnerBasisNum",{
								clist=(Table[cc[i],{i,1,len}]);
								Eqns=Transpose[A].clist-target;
								Eqns=Table[Eqns[[i]]//Together//Numerator,{i,1,len}];
								Eqns=GroebnerBasis[Eqns,clist,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions];
								muList=clist/.ListSolve[Eqns,clist][[1]];
								}

		];

		If[quietFlag==False,Print["Done...    ",SessionTime[]-ttt];ttt=SessionTime[];];

		Return[{dualBasis,muList}];
];


CharacterFunction[pointlist_,var1_]:=Module[{pointList=pointlist,var=var1,valueList,n,len,RandomBound,f,ip=1},
		len=Length[pointList];

		n=Length[var];
		RandomBound=1;
		f=RandomInteger[{-RandomBound,RandomBound},n].var;
		valueList=FullSimplify[Table[f/.pointList[[i]],{i,1,len}]];

		While[Length[Union[valueList]]<len,
					f=RandomInteger[{-RandomBound,RandomBound},n].var;
					valueList=FullSimplify[Table[f/.pointList[[i]],{i,1,len}]];
				(*	ip++; *)
		];
		(*	Print[ip]; *)
		Return[f];
		

];


Options[PartitionFunction]={QuietMode->True,SolvingMethod->"Lexicographic"}
PartitionFunction[flist_,var1_,OptionsPattern[]]:=Module[{s,g,fList=flist,var=var1,f,Eqn,len,i,list,result,basis,d,Gr,pointList,quietFlag=OptionValue[QuietMode],sMethod=OptionValue[SolvingMethod],ttt},

		ttt=SessionTime[];
		Gr=GroebnerBasis[fList,var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions];		

		Switch[sMethod,
			"Lexicographic",Eqn=GroebnerBasis[fList,var,MonomialOrder->Lexicographic,CoefficientDomain->RationalFunctions],
			"DegreeLexicographic",Eqn=Gr,
			"Normal",Eqn=fList];

		pointList=ListSolve[Eqn,var];
		pointList=Sort[Union[FullSimplify[pointList]]]; (* Remove the multiplicities of points in the list *)

		If[quietFlag==False,Print["Locus of zeros found,    ",SessionTime[]-ttt];ttt=SessionTime[];];			

		len=Length[pointList];
		f=CharacterFunction[pointList,var];	  
		g=Table[Null,{i,1,len}];

		If[quietFlag==False,Print["Distinct function found,    ",SessionTime[]-ttt];ttt=SessionTime[];];

	   (* This is the set of Lagrange functions. *)
		For[i=1,i<=len,i++,
			list=Complement[pointList,{pointList[[i]]}];		
			g[[i]]=Product[(f)-(f/.list[[j]]),{j,1,len-1}]/Product[(f/.pointList[[i]])-(f/.list[[j]]),{j,1,len-1}];
		];

		If[quietFlag==False,Print["Lagrange functions found,    ",SessionTime[]-ttt];ttt=SessionTime[];];			
		
		
		g=Table[Factor[PolynomialReduce[g[[i]],Gr,var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[2]]],{i,1,len}];  

		If[quietFlag==False,Print["Lagrange functions simplified,    ",SessionTime[]-ttt];ttt=SessionTime[];];	


		basis=QuotientRingBasis[fList,var];		
		d=Max[Multiplicities[f,Gr,var,basis,pointList]];

		If[quietFlag==False,Print["d=",d,"     ",SessionTime[]-ttt];ttt=SessionTime[];];	

		DistributeDefinitions[g,Gr,var,len,d];    
		s=Table[1-PolynomialPower[g[[i]],d,Gr,var,DegreeLexicographic],{i,1,len}]; 

		If[quietFlag==False,Print["First power...  ",SessionTime[]-ttt];ttt=SessionTime[];];


		DistributeDefinitions[s,Gr,var,len,d];    
		result=Table[1-PolynomialPower[s[[i]],d,Gr,var,DegreeLexicographic],{i,1,len}];   

		If[quietFlag==False,Print["Done...    ",SessionTime[]-ttt];ttt=SessionTime[];];	
			
		Return[{pointList,result//Factor}];

];


Multiplication[f1_,gr1_,var1_,basis1_]:=Module[{f=f1,Gr=gr1,var=var1,basis=basis1,ref,i,len,Matrix={},target},
		ref=Total[basis];

		len=Length[basis];

		For[i=1,i<=len,i++,
			target=PolynomialReduce[f*basis[[i]],Gr,var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[2]];
			AppendTo[Matrix,MonomialCoefficients[target,var,ref,DegreeLexicographic]];
		];
		Return[Matrix//Simplify];
];


Multiplicities[f1_,gr1_,var1_,basis1_,pointlist1_]:=Module[{f=f1,Gr=gr1,var=var1,basis=basis1,pointList=pointlist1,M,chf,len,\[Lambda],n},
		M=Multiplication[f,Gr,var,basis];
	
		len=Length[basis];
		n=Length[pointList];
		chf=Factor[Det[\[Lambda] IdentityMatrix[len]-M]];

		Return[Table[PoleDegree[chf,{\[Lambda],f/.pointList[[i]]}],{i,1,n}]];

];


BezoutianResidue[h1_,flist1_,var1_,dualform1_]:=Module[{h=h1,fList=flist1,Var=var1,ref,Gr,dualForm=dualform1,lambdaList},
		Gr=GroebnerBasis[fList,Var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions];
		ref=QuotientRingBasis[fList,Var]//Total;
		
		h=PolynomialReduce[h,Gr,Var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[2]];
		lambdaList=MonomialCoefficients[h,Var,ref,DegreeLexicographic];

		Return[lambdaList.dualForm//Simplify];
		
];


(* This function calculates residues from Bezoutian matrix automatically *)
BezoutianResidue[h1_,flist1_,var1_]:=Module[{h=h1,fList=flist1,Var=var1,ref,Gr,lambdaList,dualForm,dualBasis,eList,poleLocus,result={},localNum,i},
		{dualBasis,dualForm}=GlobalDual[fList,Var];
		{poleLocus,eList}=PartitionFunction[fList,Var];
		
		Gr=GroebnerBasis[fList,Var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions];
		ref=QuotientRingBasis[fList,Var]//Total;
		
		h=PolynomialReduce[h,Gr,Var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[2]];
		
		For[i=1,i<=Length[poleLocus],i++,
				localNum=PolynomialReduce[h*eList[[i]],Gr,Var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[2]];
				AppendTo[result,{poleLocus[[i]],MonomialCoefficients[localNum,Var,ref,DegreeLexicographic].dualForm}];
		];

		Return[result];
		
];


(* This function calculates the sum of residues from Bezoutian matrix *)
BezoutianGlobalResidue[h1_,flist1_,var1_]:=Module[{h=h1,fList=flist1,Var=var1,ref,Gr,lambdaList,dualForm,dualBasis,result={},i},
		{dualBasis,dualForm}=GlobalDual[fList,Var];
		
		Gr=GroebnerBasis[fList,Var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions];
		ref=QuotientRingBasis[fList,Var]//Total;
		
		h=PolynomialReduce[h,Gr,Var,MonomialOrder->DegreeLexicographic,CoefficientDomain->RationalFunctions][[2]];
		
		result=MonomialCoefficients[h,Var,ref,DegreeLexicographic].dualForm;
	
		Return[result];
		
];


(* ::Subsection::Closed:: *)
(*Finite Field Fitting*)


GFLift[s1_,p1_]:=Module[{a,b,s=s1,p=p1,r,q,n},

If[Element[s,Integers]==True,
	n=Floor[Sqrt[(p-1)/2]];
	If[s<n,Return[s]];
	a={p,0};
	b={s,1};

	While[b[[1]]!=0,
		q=Floor[a[[1]]/b[[1]]];
	r=a-b q;
	If[r[[1]]<=n&&0<Abs[r[[2]]]<=n,Return[r[[1]]/r[[2]]]];
	a=b;
	b=r;
		];
	Return[XXX];
];
Return[XXX];
];


monomialAnsatz[var_,deg_]:=(Times@@MapThread[#1^#2&,{var,Exponent[#,var]}])&/@MonomialList[(1+Total[var])^deg//Expand,var,DegreeReverseLexicographic];
monomialPickup[f_,var_]:=(Times@@MapThread[#1^#2&,{var,Exponent[#,var]}])&/@MonomialList[f,var,DegreeReverseLexicographic];


(* To fit a polynomial's coefficients in Q or Z/p.*)
polyFitting[list1_,paralist1_,var1_,degree1_,char1_]:=Module[{var=var1,paralist=paralist1,list=list1,char=char1,degree=degree1,mlist,Matrix,clist,Batchflag,sf},

Batchflag=ArrayDepth[list];

If[Length[paralist]==0,Return[{}]];
If[Batchflag==1&&Length[list]!=Length[paralist],Return[Null]];
If[Batchflag==2,If[Length[list[[1]]]!=Length[paralist],Return[Null]]];

mlist=monomialAnsatz[var,degree];

(* If[Length[mlist]>Length[paralist],Return[Null]]; *)

Matrix=(mlist/.#)&/@paralist;


Switch[Batchflag,
1,
If[char>0,clist= LinearSolve[Matrix,list,Modulus->char],clist= LinearSolve[Matrix,list]];
Return[mlist.clist],
2,
If[char>0,clist= LinearSolve[Matrix,Transpose[list],Modulus->char],clist= LinearSolve[Matrix,Transpose[list]]];
Return[mlist.clist];
];

]


(* To fit a fraction's coefficients in Q. Z/p fitting result will also be *)
fractionFitting[list1_,paralist1_,var1_,degree1_,degree2_,char1_]:=Module[{list=list1,var=var1,paralist=paralist1,numlist,denlist,char=char1,deg1=degree1,deg2=degree2,Matrix,clist,num,den},

If[Length[paralist]==0,Return[{}]];
numlist=monomialAnsatz[var,degree1];
denlist=monomialAnsatz[var,degree2];


Matrix=Table[Join[numlist,-list[[i]] denlist]/.paralist[[i]],{i,1,Length[paralist]}];



If[char>0,clist= NullSpace[Matrix,Modulus->char],clist= NullSpace[Matrix]];

If[clist=={},Return[CC],clist=clist[[1]]];
If[char>0,clist=GFLift[#,char]&/@clist]; 



num=numlist.Take[clist,Length[numlist]];
den=denlist.Take[clist,-Length[denlist]];




Return[Factor[num/den]];

];




(*polyGFLift[f1_,var1_,p1_]:=Module[{f=f1,var=var1,p=p1,list,Tri,mlist,clist},
Tri=#->1&/@var;
list=MonomialList[f1,var];

clist=list/.Tri;
mlist=list/clist;
clist=GFLift[#,p]&/@clist;

Return[clist.mlist];
] *)


(* CoefficientsLift[flist1_,numericlist1_,var1_,clist1_,deg1_,p1_]:=Module[{flist=flist1,numericlist=numericlist1,var=var1,clist=clist1,i,j,cmatrix,p=p1,deg=deg1,tri,mlist,list},
tri=#\[Rule]1&/@var;
If[Length[flist]\[NotEqual]Length[numericlist],Return[0]];

mlist=(MonomialList[flist[[1]],var])/(MonomialList[flist[[1]],var]/.tri);

(*  Each row is a polynomial in clist, evaluated for different numeric point. *)

cmatrix=Transpose[MonomialList[#,var]&/@flist]/.tri;

list=polyFitting[cmatrix,numericlist,clist,deg,p];
If[p>0,list=polyGFLift[#,clist,p]&/@list];
Return[mlist.list];

]; *)


polynomialFit[flist1_,var1_,clist1_,numericlist1_,degree1_,degree2_,p1_]:=Module[{flist=flist1,numericlist=numericlist1,var=var1,clist=clist1,i,j,ref,zz,cmatrix,p=p1,deg1=degree1,deg2=degree2,tri,mlist,list},

tri=#->1&/@var;

If[flist==Table[0,{i,1,Length[flist]}],Return[0]];

mlist=MonomialList[Total[Union[Flatten[monomialPickup[#,var]&/@flist]]],var,DegreeReverseLexicographic];

ref=zz (mlist//Total);


If[Length[flist]!=Length[numericlist],Return[0]];


(*  Each row is a polynomial in clist, evaluated for different numeric points. *)

cmatrix=Transpose[(MonomialList[#+ref,var,DegreeReverseLexicographic]&/@flist)/.zz->0]/.tri;


list=Table[fractionFitting[cmatrix[[i]],numericlist,clist,deg1,deg2,p],{i,1,Length[cmatrix]}];

Return[mlist.list];

];


End[]  
EndPackage[]
