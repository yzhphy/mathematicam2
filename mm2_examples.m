(* ::Package:: *)

<<mathematicaM2`


(* Check the options for each command *)
Options[PrimaryDecomposition]
(* We can use different number fields or algebraic extension *)
PrimaryDecomposition[{x^2+y^2},{x,y},NumberField->"QQ"]
PrimaryDecomposition[{x^2+y^2},{x,y},NumberField->"ZZ/17"]
PrimaryDecomposition[{x^2+y^2},{x,y},AlgebraicExtension->{r^2+1}]

(* Primary decomposition also works for analytic coefficients. However, it may be slow.*)
PrimaryDecomposition[{x^2-t^2+s^2 y^4+x^2,2x^2-2t^2+s^2 y^4+x^2},{x,y},NumberField->"QQ",AlgebraicExtension->{r^2+1}]


(* Groebner basis via Macaulay2. It is possible to generate the generating matrix for a Groebner basis.*)
Ideal={x^2-y^3+2 z,z^5-x^3}
{Gr,GMatrix}=M2GroebnerBasis[Ideal,{x,y,z},GeneratingMatrix->True]
Gr==Ideal.GMatrix

M2PolynomialReduce[z^7,Gr,{x,y,z}]


(* Reduce a fraction to a polynomial plus terms in the ideal, where the latter would not be shown. *)
FractionReduce[1/t,{t^2+t+2},{t}]
FractionReduce[x^4/(x^3+y^2),{x^2+x y,y^2-x^2+1},{x,y}]

(* Note that if the ideal is not big enough, a fraction may not be reduced to a polynomial. In this case, the second term is nonzero which is the residue. *)
FractionReduce[x^4/(x^3+y^2),{x^2+x y},{x,y}]


(* D=4 Double-box: Eliminate spurious variables to get a equation in (k\cdot 4) and (q\cdot 1) only. This is the 5*5 Gram relation *) 
Elimination[{-t^2+4 t x13-4 x13^2+4 x14^2,-t^2+4 t x21-4 x21^2+4 x24^2,-4 t x13 x21+s (-x13^2+x14^2-2 x13 x21-x21^2+2 x14 x24+x24^2)},{x14,x24,x13,x21},{x14,x24}]
(* Find the variety defined by parameters *)
(* Sphere *)
Implicitization[{(1-t^2)/(1+t^2),2 t/(1+t^2)},{t},{x,y}]
(* General conic *)
Implicitization[{((1-t)^2 x1+2t(1-t)w x2+t^2 x3)/((1-t)^2+2t(1-t)w+t^2 ),((1-t)^2 y1+2t(1-t)w y2+t^2 y3)/((1-t)^2+2t(1-t)w+t^2 )},{t},{x,y}]


(* This generates the projective ideal of a given ideal *)
ProjectiveIdeal[{x^2+y^2-1,x^3-y^2},{x,y},z]


(* This generate the equtions for the singular points on a variety. The exact location of singular points can be determined by solving these equations. *)
M2SingularLocus[{x^2-y^2},{x,y},Projective->False]
Solve[%==Table[0,{i,1,Length[%]}],{x,y}]
M2SingularLocus[{y^2-x^3+x^2},{x,y},Projective->False]
Solve[%==Table[0,{i,1,Length[%]}],{x,y}]

(* Usually we need to consider the singularity at infinity. So we just make the ideal projective. *)
M2SingularLocus[ProjectiveIdeal[{y^2-x^4-x^2-1},{x,y},z],{x,y,z},Projective->True]
Solve[%==Table[0,{i,1,Length[%]}],{x,y,z}]


(* Insersection of two Ideals *)
M2Intersect[{x-y},{x+y},{x,y}]
M2Intersect[{x^2-y^2+1,z-y^2},{x+y,z^2-x},{x,y,z}]
(*Intersection of several Ideals. This may be slow for the case with a large number of Ideals.*)
IntersectIdeal[{{x^2-y^2+7,z-y^2},{x+y,z^2-x},{x-y,z+x}},{x,y,z}]
IntersectIdeal[{{x^2-y^2+7,z-y^2},{x+y,z^2-x},{x-y,z+x}},{x,y,z},NumberField->"ZZ/7"]


(* Arithmetic genus of an Ideal, only for projective variety. Note that Arithmetic genus is larger than geometry genus for singular curves. *)

M2ArithmeticGenus[ProjectiveIdeal[{y^2-x^4-x^2-1},{x,y},z],{x,y,z}]
M2ArithmeticGenus[ProjectiveIdeal[{y^2-x^3-1},{x,y},z],{x,y,z}]

(* Dimension of a variety *)
M2Dim[{x-y},{x,y,z,w}]
M2Dim[{x^2-y^2+1,x^2-z^2},{x,y,z}]

(* Degree of a varity *)
M2Degree[{x^2-y^2+1,x^2-z^2},{x,y,z}]


(* Chinese remainder theorem *)
f=x^4;
Ideal={x^2-y^2+1,z^3-y^3}
r=PolynomialReduce[f,Ideal,{x,y,z},MonomialOrder->Lexicographic][[2]]
{I1,I2}=PrimaryDecomposition[Ideal,{x,y,z}]
r1=PolynomialReduce[f,I1,{x,y,z},MonomialOrder->Lexicographic][[2]]
r2=PolynomialReduce[f,I2,{x,y,z},MonomialOrder->Lexicographic][[2]]

(* Recover r from r1, r2 *)
CRT2[{r1,I1},{r2,I2},{x,y,z},MonomialOrder->Lexicographic]
%[[1]]==r


(* Chinese remainder theorem for several polynomials *)
f=x^4;
Ideal={x^2-y^2,z^3-y^3};
r=PolynomialReduce[f,Ideal,{x,y,z},MonomialOrder->Lexicographic][[2]]
{I1,I2,I3,I4}=PrimaryDecomposition[Ideal,{x,y,z}]
r1=PolynomialReduce[f,I1,{x,y,z},MonomialOrder->Lexicographic][[2]];
r2=PolynomialReduce[f,I2,{x,y,z},MonomialOrder->Lexicographic][[2]];
r3=PolynomialReduce[f,I3,{x,y,z},MonomialOrder->Lexicographic][[2]];
r4=PolynomialReduce[f,I4,{x,y,z},MonomialOrder->Lexicographic][[2]];
{r1,r2,r3,r4}
(* Recover r from r1, r2, r3 and r4 *)
 CRT[{r1,r2,r3,r4},{I1,I2,I3,I4},{x,y,z},MonomialOrder->Lexicographic]
%==r


(* Quadratic Form Reduction *) 
poly=2+3 x1+x1^2-3 x2-4 x1 x2-5 x2^2;
QuadraticForm[poly,{x1,x2},NewVariables->{y1,y2},Normalization->False]
QuadraticForm[poly,{x1,x2},NewVariables->{y1,y2},Normalization->True]
QuadraticForm[poly,{x1,x2},NewVariables->{y1,y2},Normalization->True,RealCondition->False]

(* Note that a quadratic form can be degenerate. *)
poly=x1^2+x2^2+x3^2-2x4^2-2 x1 x2+2x1 x3-2x1 x4+2 x2 x3-4x2 x4;
QuadraticForm[poly,{x1,x2,x3,x4},NewVariables->{y1,y2,y3,y4},Normalization->False]
QuadraticForm[poly,{x1,x2,x3,x4},NewVariables->{y1,y2,y3,y4},Normalization->True]


(* Find all irreducible factos of a polynomial, over C. *)
Factorize[x^3+y^3+z^3-3 x y z,{x,y,z}]
Product[%[[i]],{i,1,Length[%]}]//FullSimplify

Factorize[w^4+x^4+4 w^2 x y+6 x^2 y^2+y^4+4 w x^2 z+4 w y^2 z+6 w^2 z^2+4 x y z^2+z^4,{x,y,z,w}]
Product[%[[i]],{i,1,Length[%]}]//FullSimplify

Factorize[x^4+y^4-x^2 y^2,{x,y}]
Product[%[[i]],{i,1,Length[%]}]//FullSimplify

(* Irreducible polynomial examples *)
(* It may take long time to determine a polynomial is irreducible. *)
Factorize[x^2+y^2+z^2,{x,y,z}]
Factorize[x^4+y^4+z^4+4x y z^2,{x,y,z}]


MultiResidue[1,{z1,z2},{z1,z2},{0,0}]
MultiResidue[z2,{z1,(z1-z2)(z1+z2)},{z1,z2},{0,0}]
MultiResidue[z2,{z1^2,z2-z1},{z1,z2},{0,0}]


MultiResidue[1,{(x^2+y-a^2)^3,y^3},{x,y},{a,0}]
BezoutianResidue[1,{(x^2+y-a^2)^3,y^3},{x,y}]


F=y^2-x(x-a2)(x-a3)(x-a4);
fList={D[F,x],D[F,y],F};


syz1=M2Syzygy[fList,{x,y},OptionalInput->"Algorithm=>Sugarless,Strategy=>{}",MinimalGenerators->True];
syz2={{-fList[[2]],fList[[1]],0},{-fList[[3]],0,fList[[1]]},{0,-fList[[3]],fList[[2]]}}//Transpose;

G1=M2GroebnerBasis[syz1,{x,y},OptionalInput->"Algorithm=>Sugarless,Strategy=>LongPolynomial"];
G2=M2GroebnerBasis[syz2,{x,y},OptionalInput->"Algorithm=>Sugarless,Strategy=>LongPolynomial"];

G1==G2//Simplify
