				MathematicaM2 package
			               ver 0.98

This is a Mathematica package by Yang Zhang, designed
to do the following duties,

1. Call Macaulay2 to carry out computations in algebraic geometry,
2. Do various polynomial calculation in Mathematica, like the
reduction of quadratic form and the computation of resultants,
3. Two-loop unitarity analysis.

The steps for installation are,

1. Install Macaulay2.
2. Make a new directory for temporary files.
3. Edit the file "init.m" of Mathematica. The path of "init.m" can be
found by the Mathematica command,  

FileNameJoin[{$UserBaseDirectory, "Kernel", "init.m"}].

In "init.m", add the following lines:

(* path of MathematicaM2 package *)
AppendTo[$Path,"/Users/cosmology/mathematics/CAG/mathematicam2/"]

(* path of Macaulay2 *)
Global`M2path = "/Applications/Macaulay2-1.4/bin/M2";

(* path of MathematicaM2 temporary files *) 
Global`Datapath = "~/exchange";

Replace the paths by your own paths, respectively.

Then you are ready to run several examples.
